`timescale 1ns / 1ns
module adpram #(parameter DWA=16
,parameter AWA=10
,parameter DWB=32
,parameter AWB=9
)(input [0:0] clka
,input [AWA-1:0] addra
,input [DWA-1:0] dataa
,input [0:0] wena
,input [0:0] clkb
,input [AWB-1:0] addrb
,output [DWB-1:0] datab
,output [0:0] error
,input [0:0] enb
);
`define max(a,b) {(a) > (b) ? (a) : (b)}
`define min(a,b) {(a) < (b) ? (a) : (b)}
function integer clog2;
    input integer value;
    begin
        value = value-1;
        for (clog2=0; value>0; clog2=clog2+1)
            value = value>>1;
    end
endfunction

localparam SIZEA=2**AWA;
localparam SIZEB=2**AWB;
localparam maxSIZE = `max(SIZEA, SIZEB);
localparam maxWIDTH = `max(DWA, DWB);
localparam minWIDTH = `min(DWA, DWB);
localparam RATIO = maxWIDTH / minWIDTH;
localparam log2RATIO = clog2(RATIO);
reg [minWIDTH-1:0] ram [0:maxSIZE-1];
/*
wire [log2RATIO-1:0] lsbaddr [0:RATIO-1];
genvar i;
generate for (i=0; i<RATIO; i=i+1) begin: lsbA
	assign lsbaddr[i]=i;
	always @(posedge clka) begin
		if (wena)
			ram[{addra,lsbaddr[i]}]<=dataa[(i+1)*minWIDTH-1-:minWIDTH];
	end
end
endgenerate
*/
/*
wire [15:0] ram0=ram[0];
wire [15:0] ram1=ram[1];
wire [15:0] ram2=ram[2];
wire [15:0] ram3=ram[3];
wire [15:0] ram4=ram[4];
wire [15:0] ram5=ram[5];
wire [15:0] ram6=ram[6];
wire [15:0] ram7=ram[7];
wire [15:0] ram8=ram[8];
wire [15:0] ram9=ram[9];
wire [15:0] ram10=ram[10];
wire [15:0] ram11=ram[11];
wire [15:0] ram12=ram[12];
wire [15:0] ram13=ram[13];
wire [15:0] ram14=ram[14];
wire [15:0] ram15=ram[15];
*/
always @(posedge clka) begin: ramwrite
	integer i;
	reg [log2RATIO-1:0] lsbaddr;
	for (i=0; i< RATIO; i= i+ 1) begin : write1
		lsbaddr = i;
		if (wena) begin
			if (log2RATIO==0)
				ram[addra] <= dataa[(i+1)*minWIDTH-1 -: minWIDTH];
			else
				ram[{addra, lsbaddr}] <= dataa[(i+1)*minWIDTH-1 -: minWIDTH];
		end
	end
end
reg [DWB-1:0] datab_r=0;
always @(posedge clkb) begin
	datab_r<=ram[addrb];
end
assign datab=enb ? datab_r :0;
endmodule
