module alatch #(parameter DWIDTH=16)(input clk
,input [DWIDTH-1:0] datain
,output [DWIDTH-1:0] dataout
);
(* ASYNC_REG = "TRUE" , KEEP="TRUE"*) reg [DWIDTH-1:0] datasr1=0;
(* ASYNC_REG = "TRUE" , KEEP="TRUE"*) reg [DWIDTH-1:0] datasr2=0;
(* ASYNC_REG = "TRUE" , KEEP="TRUE"*) reg [DWIDTH-1:0] datasr3=0;
always @(posedge clk) begin
	datasr1<=datain;
	datasr2<=datasr1;
	datasr3<=datasr2;
end
assign dataout=datasr3;
endmodule
