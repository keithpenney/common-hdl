module async#(parameter WIDTH=1)
(input [WIDTH-1:0] asig
,input clk
,output [WIDTH-1:0] ssig_val
,output [WIDTH-1:0] ssig
);
genvar iw;
generate
	for (iw=0;iw<WIDTH;iw=iw+1) begin
		asig_1 asig_i(.clk(clk),.asig(asig[iw]),.ssig(ssig[iw]),.ssig_val(ssig_val[iw]));
	end
endgenerate
endmodule


`timescale 1ns/1ps
module asig_1
(input clk
,input asig
,output ssig
,output ssig_val
);

(* ASYNC_REG = "TRUE" *) reg sig_p=0;
(* ASYNC_REG = "TRUE" *) reg [2:0] sig_sr=0;
always @(posedge clk) begin
	if (asig)
		sig_p<=1'b1;
	else
		sig_p<=1'b0;
end
always @(posedge clk)
	sig_sr <= {sig_sr[1:0], sig_p};
assign ssig= (~sig_sr[2] & sig_sr[1]);
assign ssig_val = sig_sr[2];
endmodule
