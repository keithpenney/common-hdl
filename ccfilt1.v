`timescale 1ns / 10ps
module ccfilt1(input clk
,(* mark_debug = "true" *) input signed [INTEWIDTH-1:0] intestream
,(* mark_debug = "true" *) input [GW-1:0] integate
,(* mark_debug = "true" *) input [SHIFTWIDTH-1:0] shift  // should be cic_order*wavesampper
,(* mark_debug = "true" *) output signed [RWI-1:0] dout
,(* mark_debug = "true" *) input reset
,(* mark_debug = "true" *) output [GW-1:0] gout
);
function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction
//.NCHAN(NCHAN),.ADCWIDTH(ADCWIDTH),.CIC_PERIOD(CIC_PERIOD),.CIC_ORDER(CIC_ORDER),.SAMPLEPERMAX(SAMPLEPERMAX),.DAVR(DAVR)
parameter NCHAN=2;
//parameter ADCWIDTH=16;
parameter CIC_PERIOD=132;
parameter CIC_ORDER=2;
parameter SAMPLEPERMAX=32;
parameter RWI=20;
wire [15:0] rwi=RWI;
parameter GW=1;
parameter NORMALIZE=0;
parameter INTEWIDTH=0;

parameter SAMPADD=clog2(SAMPLEPERMAX**CIC_ORDER);
//localparam RWISAMP=RWI+SAMPADD;
//localparam DAVR=RWI-ADCWIDTH;
parameter CICADD=clog2(CIC_PERIOD**CIC_ORDER);
localparam SHIFTWIDTH=clog2(CICADD+SAMPADD);//CIC_ORDER*clog2(SAMPLEPERMAX); // for cic
localparam MULTIWIDTH=18;
wire [15:0] cicadd=CICADD;
wire [15:0] sampadd=SAMPADD;

localparam NORMALIZEWIDTH=MULTIWIDTH-2; //MULTIWIDTH-1-clog2(2**(17)/(CIC_PERIOD**CIC_ORDER));
wire [15:0] normalizewidth=NORMALIZEWIDTH;
localparam NORMALIZEFACTOR=(2**(NORMALIZEWIDTH+CICADD)-1)/(CIC_PERIOD**CIC_ORDER);
wire [MULTIWIDTH-1:0] tnorm=NORMALIZEFACTOR;
wire [GW-1:0] valid2w;
reg [GW-1:0] valid2=0;
(* mark_debug = "true" *) reg signed [INTEWIDTH-CICADD-1:0] d2=0;
(* mark_debug = "true" *) reg signed [INTEWIDTH-CICADD-1:0] d2p=0;
(* mark_debug = "true" *) wire signed [INTEWIDTH-1:0] d2w;
doublediff1 #(.dw(INTEWIDTH), .dsr_len(NCHAN),.gw(GW))
diff(.clk(clk),.d_in(intestream), .g_in(integate), .d_out(d2w), .g_out(valid2w),.reset(reset));
always @(posedge clk) begin
	valid2 <= valid2w;
	d2  <= d2w>>>CICADD;
end
(* mark_debug = "true" *) reg signed [RWI-1:0] d3=0;
(* mark_debug = "true" *) reg signed [RWI-1:0] d3d=0;
//reg signed [RWISAMP-1:0] d3d=0;
(* mark_debug = "true" *) wire signed [MULTIWIDTH-1:0] normalizefactor=$signed(NORMALIZEFACTOR);
(* mark_debug = "true" *) wire signed [INTEWIDTH-CICADD+MULTIWIDTH-1:0] multi = (d2) * normalizefactor;
(* mark_debug = "true" *) reg signed [INTEWIDTH-CICADD+MULTIWIDTH-1:0] multir=0;
(* mark_debug = "true" *) wire [31:0] multiwidth=INTEWIDTH+MULTIWIDTH;
generate
if (NORMALIZE==1) begin
	always @(posedge clk) begin
		multir <= multi;
		d3<= multir >>> (NORMALIZEWIDTH+shift);
		d3d<=d3;
	end
end
else begin
	always @(posedge clk) begin
		d2p<= d2 >>> (shift);
		d3<= d2p;
		d3d<=d3;
	end
end
endgenerate
//wire [15:0] msb=RWI+DAVR-1;
//wire [15:0] davr=DAVR;
//assign dout = d3d[RWI+DAVR:DAVR+1];
assign dout = d3d;
reg_delay1 #(.dw(GW),.len(3)) reg_delay(.clk(clk),.gate(1'b1),.din(valid2),.dout(gout),.reset(reset));
endmodule
