`timescale 1ns / 1ns
module checkreadresetbuf#(parameter DWA=32
,parameter AWA=13
,parameter DWB=16
,parameter auto_flip = 1
)
(input clkA
,input [DWA-1:0] dataA
,input weA
,output [AWA-1:0] addrA
,input flip
,input clkB
,output full
,input [AWB-1:0] addrB
,output [DWB-1:0] dataB
,input enB
,output [31:0] bufcnt
);
function integer clog2;
    input integer value;
    begin
        value = value-1;
        for (clog2=0; value>0; clog2=clog2+1)
            value = value>>1;
    end
endfunction
localparam AWB=clog2(DWA*(2**AWA)/DWB);

reg run=0;
reg flip_r=0;
reg [AWA:0] addrA_r=0;
reg flip_r_d=0;
reg [31:0] bufcnt_r=0;
always @(posedge clkA) begin
	flip_r_d<=flip_r;

	if (flip)
		flip_r<=1'b1;
	else if (weA)
		flip_r<=1'b0;

	if (flip_r&~flip_r_d)
		addrA_r<=0;
	else if (~full&weA)
		addrA_r<=addrA_r+1'b1;

	if (full&flip_r&~flip_r_d)
		bufcnt_r<=bufcnt_r+1;
end
assign bufcnt=bufcnt_r;
assign addrA=addrA_r;//[AWA-1:0];
assign full=addrA_r[AWA];

wire [DWB-1:0] dataBw;
reg [DWB-1:0] dataBr=0;
adpram #(.DWA(DWA),.AWA(AWA),.DWB(DWB),.AWB(AWB))
adpram (.clka(clkA),.addra(addrA),.dataa(dataA),.wena(weA&(~full))
,.clkb(clkB),.addrb(addrB),.datab(dataBw),.enb(enB)
,.error(error)
);
always @(posedge clkB)begin
	if (enB)
		dataBr<=dataBw;
end
assign dataB=dataBr;
endmodule
