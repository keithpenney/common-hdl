`timescale 1ns / 1ns

// Post-clipping harmonic-reject filter, suited for carriers near f_sample*2/11,
// e.g., APEX with 102.14 MHz clock.
// Zero-multiplier footprint
module clipfilt(
	input clk,  // timespec 8.0 ns
	input signed [15:0] d_in,
	output signed [15:0] d_out
);

// Universal definition; note: old and new are msb numbers, not bit widths.
`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})

// z-transform filter gain
//  0.5*(0.5+z.^(-1)-0.5*z.^(-3))
// see octave/clipfilt.m
reg signed [15:0] d1=0, d2=0;  // input z^(-1) registers
reg signed [17:0] s1=0; // sum registers
reg signed [17:0] s2=0;

always @(posedge clk) begin
	d1 <= d_in;
	d2 <= d1;
	s1 <= (d_in<<<1) - d2;
	s2 <= d_in + s1;
end
assign d_out = s2[17:2];

endmodule
