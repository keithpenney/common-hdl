`timescale 1ns / 1ns

module cordicg_tb();

reg clk;
initial clk=0;
integer cc;
initial begin
	$dumpfile("cordicg.vcd");
	$dumpvars(5,cordicg_tb);
	for (cc=0; cc<50; cc=cc+1) begin
		clk=0; #10;
		clk=1; #10;
	end
end
parameter WIDTH=20;
parameter ZWIDTH=WIDTH+1;
parameter NSTAGE=20;
/*reg [0:0] opin=1'b1;
reg signed [WIDTH-1:0] xin=6000;
reg signed [WIDTH-1:0] yin=-32000;
reg signed [ZWIDTH-1:0] phasein=0;
*/
reg [0:0] opin=1'b0;
reg signed [WIDTH-1:0] xin=79590;
reg signed [WIDTH-1:0] yin=0;
reg signed [ZWIDTH-1:0] phasein=161316;
wire signed [WIDTH-1:0] xout, yout;
wire [ZWIDTH-1:0] phaseout;
wire signed [WIDTH-1:0] xout1, yout1;
wire [ZWIDTH-1:0] phaseout1;
cordicg1 #(.WIDTH(WIDTH),.NSTAGE(NSTAGE),.NORMALIZE(1),.NRIDER(1))
//cordicg1(.clk(clk),.opin(opin),.phasein(phasein),.phaseout(phaseout),.xin(xin),.xout(xout),.yin(yin),.yout(yout));
cordicg1(.clk(clk),.opin(opin),.phasein({phasein,phasein}),.phaseout({phaseout,phaseout1}),.xin({20'd30000,xin}),.xout({xout,xout1}),.yin({20'd52342,yin}),.yout({yout,yout1}));
/*>>> 79590*exp(1j*161316./2**21*2*pi)
(70473.820147932202+36986.602625233172j)
>>> (30000+52342j)*exp(1j*161316./2**21*2*pi)
(2239.7518510869631+60288.211746953923j)
*/
endmodule
