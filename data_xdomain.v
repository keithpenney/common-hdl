// clk_out must be more than twice as fast as the gate_in rate.
`timescale 1ns / 1ns
module data_xdomain #(parameter DWIDTH=32)(
input clkin
,input  gatein
,input clkout
,input [DWIDTH-1:0] datain
,output [DWIDTH-1:0] dataout
,output gateout
);


reg [DWIDTH-1:0] dataout_r=0;
reg gateout_r=0;
assign dataout=dataout_r;
assign gateout=gateout_r;
reg [DWIDTH-1:0] data_latch=0;
always @(posedge clkin) begin
	if (gatein) begin
		data_latch <= datain;
	end
end

wire gatex;
flag_xdomain flag_xdomain(.clk1(clkin),  .flagin_clk1(gatein),.clk2(clkout), .flagout_clk2(gatex));

// It can be argued that the final pipeline stage is not needed,
// but then you'd need oddball timing constraints on the data path.
reg [DWIDTH-1:0] data_pipe=0;
always @(posedge clkout) begin
	data_pipe <= data_latch;
	gateout_r <= gatex;
	if (gatex) begin
		dataout_r <= data_pipe;
	end
end

endmodule
