`timescale 1ns / 1ns

module double_inte1(clk,in,out,reset);
parameter DWI=16;  // data width in
parameter DWO=28;  // data width out n bits more the in, 2^n should be more than the cic factor^2 in sel case, 47^2=2209 adding 12 bit seems a little limited
(* mark_debug = "true" *)	input clk;  // timespec 8.4 ns
(* mark_debug = "true" *)	input signed [DWI-1:0] in;  // possibly muxed
(* mark_debug = "true" *)	output signed [DWO-1:0] out;
(* mark_debug = "true" *)	input reset;  // reset integrator to 0

(* mark_debug = "true" *)reg signed [DWO-1:0] int1=0, int2=0;
reg ignore=0;
always @(posedge clk) begin
	//{int1,ignore} <= reset ? 0 : ($signed({int1,1'b1}) +in);
	int1 <=  reset ? 0 : (int1 + in);
	int2 <=  reset ? 0 : (int2 + int1);
end
assign out = int2;

endmodule
