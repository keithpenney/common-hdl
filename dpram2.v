`timescale 1ns / 1ns
// Dual port memory with independent clocks, port B is read-only
// Altera and Xilinx synthesis tools successfully "find" this as block memory
module dpram2(input clka
,input wena
,input [AW-1:0] addra
,input [DW-1:0] dina
,input clkb
,input [AW-1:0] addrb
,output [DW-1:0] doutb
);
parameter AW=8;
parameter DW=8;
parameter SZ=(32'b1<<AW)-1;





reg [DW-1:0] mem[SZ:0];
reg [AW-1:0] ala=0, alb=0;

`ifdef SIMULATE
integer k=0;
initial
begin
	for (k=0;k<sz+1;k=k+1)
	begin
		mem[k]=0;
	end
end
`endif




assign douta = mem[ala];
assign doutb = mem[alb];
always @(posedge clka) begin
	ala <= addra;
	if (wena) mem[addra]<=dina;
end
always @(posedge clkb) begin
	alb <= readrst ? 0 : (alb+inc);
end
assign addrb_out=alb;

endmodule
