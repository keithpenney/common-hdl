`timescale 1ns / 100ps
// parallel read/write pointer, binary and gray code
// binary code for memory index
// gray code for empty and full check
// buffer two stages to cross domain between r/w
// http://www.idc-online.com/technical_references/pdfs/electronic_engineering/FIFO_Pointers.pdf
module fifo0 #(parameter DW=16
,parameter BRAM=1
,parameter AW=8
,parameter SIM=0
,parameter SAMECLKDOMAIN=0
,parameter RSTFULL=1
,parameter RSTEMPTY=1
)(input rclk
,input wclk
,input rst
,input [DW-1:0] din
,input wr_en
,output [DW-1:0] dout
,input rd_en
,output full
,output empty
,output doutvalid
);
localparam SZ=(32'b1<<AW)-1;
wire reset=rst;
wire [DW-1:0] wdata=din;
wire wen=wr_en;
wire [DW-1:0] rdata;
assign dout=rdata;
wire ren=rd_en;
reg rdatavalid=0;
assign doutvalid=rdatavalid;
reg wreset=0;
reg rreset=0;
reg [AW:0] bufcntw=0;
reg [AW:0] bufcntr=0;
reg [AW:0] bufcntw_r=0;
reg [AW:0] bufcntr_r=0;
reg full_r=0;
wire full_w;
reg empty_r=1;
wire empty_w;
reg [AW:0] waddr_r=0;
wire [AW:0] waddr_next=waddr_r+1'b1;
wire [AW:0] waddr_next2=waddr_next+1'b1;
reg [AW:0] raddr_r=0;
wire [AW:0] raddr_next=raddr_r+1'b1;
wire [AW:0] raddr_next2=raddr_next+1'b1;
wire [AW:0] raddr_next3=raddr_next2+1'b1;
wire [AW-1:0] waddr=waddr_r[AW-1:0];
wire [AW-1:0] raddr=raddr_r[AW-1:0];
reg [AW:0] waddr_x=0;
reg [AW:0] waddr_x2=0;
reg [AW:0] waddr_x3=0;
reg [AW:0] raddr_x=0;
reg [AW:0] waddr_x4=0;
reg [AW:0] waddr_x5=0;
reg [AW:0] raddr_x2=0;
reg [AW:0] raddr_x3=0;
reg [AW:0] raddr_x4=0;
reg [AW:0] raddr_x5=0;
reg emptyc=0;
reg fullc=0;
always @(posedge wclk or posedge reset) begin
	if (reset)
		wreset<=1'b1;
	else
		if (wreset)
			wreset<=1'b0;
end
always @(posedge rclk or posedge reset) begin
	if (reset)
		rreset<=1'b1;
	else
		if (rreset)
			rreset<=1'b0;
end

always @(posedge wclk) begin
	if (wreset) begin
		waddr_r<=0;
		full_r<=RSTFULL;
	end
	else begin
		if (wen&~full) begin
			waddr_r<=waddr_next;
		end
		full_r<=full_w;
	end
end

always @(posedge rclk) begin
	if (rreset) begin
		raddr_r<=0;
		empty_r<=RSTEMPTY;
	end
	else begin
		if (ren&~empty_w) begin
			raddr_r<=raddr_next;
		end
		empty_r<=empty_w;
	end
	rdatavalid<=ren&~empty_w;
end

always @(posedge rclk) begin
	waddr_x<=waddr_next;
	waddr_x2<=waddr_x;
	waddr_x3<=waddr_x2;
	waddr_x4<=waddr_x3;
	waddr_x5<=waddr_x4;
end
always @(posedge wclk) begin
	raddr_x<=raddr_next;
	raddr_x2<=raddr_x;
	raddr_x3<=raddr_x2;
	raddr_x4<=raddr_x3;
	raddr_x5<=raddr_x4;
end

wire [AW:0] waddr_xsel=waddr_x2;
wire [AW:0] raddr_xsel=raddr_x2;
always @(waddr_xsel or raddr) begin
	bufcntr=waddr_xsel-raddr;
end
always @(waddr or raddr_xsel) begin
	bufcntw=waddr-raddr_xsel;
end
always @(posedge rclk) begin
	bufcntr_r<=bufcntr;
end
always @(posedge wclk) begin
	bufcntw_r<=bufcntw;
end
always @(bufcntr) begin
	if (bufcntr<= 1) //(wen ? 2 : 0))
		emptyc=1'b1;
	else if (bufcntr>=0)
		emptyc=1'b0;
end
always @(bufcntw) begin
	if (bufcntw>=(SZ-2))
		fullc=1'b1;
	else if (bufcntw< (SZ-3))
		fullc=1'b0;
end

wire [DW-1:0] rdata_w;
dpram0 #(.AW(AW),.DW(DW),.BUFIN(0),.BUFOUT(BRAM),.SIM(SIM))mem(.reset(reset)
,.clkb(rclk),.addrb(raddr),.doutb(rdata_w),.renb(ren&~empty_w)
,.clka(wclk),.addra(waddr),.dina(wdata),.wena(wen&~full_w),.douta());

wire full_2=(waddr_r=={~raddr_x2[AW],raddr_x2[AW-1:0]}) ;
//wire empty_x4 = bufcntr_r<=2 ;//(waddr_x4==raddr_r);
wire emptynext_0= waddr_r==raddr_r+1;
wire empty_1=waddr_x==raddr_r;
reg empty_2=0;
always @(posedge rclk) begin
	empty_2<=waddr_x2==raddr_r;
end
reg full_x4_r=0;
wire full_x4;
always @(posedge wclk) begin
	full_x4_r<=full_x4;
end


wire empty_0 = (waddr_r==raddr_r);
wire empty_x4 = (waddr_x4==raddr_r);
wire empty_x5 = (waddr_x5==raddr_r) | (ren &(waddr_x5==raddr_next));
wire empty_x2 = (waddr_x2==raddr_r) | (ren &(waddr_x2==raddr_next));
wire empty_xsel = (waddr_xsel==raddr_r) | (ren &(waddr_xsel==raddr_next));
wire full_0=  (waddr_r=={~raddr_r[AW],raddr_r[AW-1:0]});
assign full_x4= (waddr_r=={~raddr_x4[AW],raddr_x4[AW-1:0]});
wire full_x5= (waddr_r=={~raddr_x5[AW],raddr_x5[AW-1:0]}) | (wen & (waddr_next=={~raddr_x5[AW],raddr_x5[AW-1:0]}));
wire full_x2= (waddr_r=={~raddr_x2[AW],raddr_x2[AW-1:0]}) | (wen & (waddr_next=={~raddr_x2[AW],raddr_x2[AW-1:0]}));
wire full_xsel= (waddr_next2=={~raddr_xsel[AW],raddr_xsel[AW-1:0]}) | (wen & (waddr_next=={~raddr_xsel[AW],raddr_xsel[AW-1:0]}));
assign  empty_w= rreset | (SAMECLKDOMAIN ? empty_0 : empty_xsel);
assign full_w=   wreset | (SAMECLKDOMAIN ? full_0  : full_xsel);//fullc;//full_2g);
assign #0.1 empty= SAMECLKDOMAIN ? empty_w : empty_r;
assign #0.1 full=  SAMECLKDOMAIN ? full_w  : full_r ;
//wire [AW-1:0] raddr=raddr_r2[AW-1:0];
assign rdata=rdatavalid ? rdata_w : {DW{1'bz}};
//assign rdata=rdata_w;//empty_w ? rdata : rdata_w;
//assign rdatavalid=BRAM?(ren&~empty_r):(ren&~empty_w);
endmodule
