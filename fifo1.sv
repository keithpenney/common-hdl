`timescale 1ns / 1ns
module fifo1(input  wclk
,input [DW-1:0] wdata
,input wen
,input rclk
,output [DW-1:0] rdata
,input ren
,output full
,output empty
,output rgate
);

parameter DW=16;
parameter AW=8;
wire [7:0] aw=AW;
reg full_r=0;
reg empty_r=1;
assign full=full_r;
assign empty=empty_r;
reg [AW-1:0] waddr=0;
wire [AW-1:0] waddr_next=waddr+1'b1;
always @(posedge wclk) begin
	if (wen&&~full) begin
		waddr  <= waddr_next;
	end
end
reg [AW-1:0] raddr={AW{1'b1}};//{1'b0,{(AW-1){1'b0}}};
wire [AW-1:0] raddr_next=  raddr+1'b1;
reg rgate_r=0;
reg rgate_rd=0;
always @(posedge rclk) begin
//always @(*) begin
	if (ren&&~empty) begin
		raddr <= raddr_next;
	end
end
always @(posedge rclk) begin
	rgate_r<=ren&&~empty;
	rgate_rd<=rgate_r;
end
assign rgate=rgate_rd;

dpram #(.aw(AW), .dw(DW))
dpram(.clkb(rclk)
,.addrb(raddr[AW-1:0])
,.doutb(rdata)
,.clka(wclk)
,.addra(waddr[AW-1:0])
,.dina(wdata)
,.wena(wen)
);

reg [AW-1:0] raddr_inwdomain=0;
always @(posedge wclk) begin
	raddr_inwdomain <= raddr;
end
always @(*) begin
	full_r = raddr_inwdomain==waddr;
end
reg [AW-1:0] waddr_inrdomain=0;
always @(posedge rclk) begin
	waddr_inrdomain <= waddr;
end
always @(*) begin
	empty_r = waddr_inrdomain==raddr_next;
end

endmodule
