create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fifocore0
set_property -dict {
CONFIG.Fifo_Implementation {Common_Clock_Block_RAM}
CONFIG.Input_Depth {32}
CONFIG.Output_Depth {32}
CONFIG.Input_Data_Width {8}
CONFIG.Output_Data_Width {8}
CONFIG.Reset_Type {Asynchronous_Reset}
CONFIG.Use_Dout_Reset {true}
CONFIG.Data_Count_Width {5}
CONFIG.Write_Data_Count_Width {5}
CONFIG.Read_Data_Count_Width {5}
CONFIG.Full_Threshold_Assert_Value {30}
CONFIG.Full_Threshold_Negate_Value {29}
} [get_ips fifocore0]
generate_target {instantiation_template} [get_files fifocore0.xci]
generate_target all [get_files  fifocore0.xci]
export_ip_user_files -of_objects [get_files fifocore0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] fifocore0.xci]
#CONFIG.Fifo_Implementation {Common_Clock_Shift_Register}
#CONFIG.Fifo_Implementation {Common_Clock_Distributed_RAM}
