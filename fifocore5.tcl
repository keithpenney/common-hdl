create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fifocore5
set_property -dict {
CONFIG.Input_Depth {32}
CONFIG.Output_Depth {32}
CONFIG.Input_Data_Width {8}
CONFIG.Output_Data_Width {8}
CONFIG.Fifo_Implementation {Independent_Clocks_Distributed_RAM}
CONFIG.Reset_Type {Asynchronous_Reset}
CONFIG.Enable_Reset_Synchronization {true}
CONFIG.Full_Flags_Reset_Value {1}
CONFIG.Use_Dout_Reset {true}
CONFIG.Dout_Reset_Value {0}
CONFIG.Write_Data_Count {true}
CONFIG.Read_Data_Count {true}
CONFIG.Data_Count_Width {5}
CONFIG.Write_Data_Count_Width {5}
CONFIG.Read_Data_Count_Width {5}
CONFIG.Programmable_Full_Type {No_Programmable_Full_Threshold}
CONFIG.Programmable_Empty_Type {No_Programmable_Empty_Threshold}
} [get_ips fifocore5]
generate_target {instantiation_template} [get_files fifocore5.xci]
generate_target all [get_files  fifocore5.xci]
export_ip_user_files -of_objects [get_files fifocore5.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] fifocore5.xci]
#CONFIG.Fifo_Implementation {Common_Clock_Shift_Register}
#CONFIG.Fifo_Implementation {Common_Clock_Distributed_RAM}
#CONFIG.Fifo_Implementation {Common_Clock_Block_RAM}
#CONFIG.Fifo_Implementation {Common_Clock_Builtin_FIFO}
#CONFIG.Programmable_Full_Type {Multiple_Programmable_Full_Threshold_Constants}
#CONFIG.Empty_Threshold_Assert_Value {2}
#CONFIG.Empty_Threshold_Negate_Value {3}
#CONFIG.Full_Threshold_Assert_Value {29}
#CONFIG.Full_Threshold_Negate_Value {28}
#CONFIG.Performance_Options {Standard_FIFO}
#CONFIG.Enable_Safety_Circuit {false}
#CONFIG.Performance_Options {First_Word_Fall_Through}
