`timescale 1ns / 1ns

module freq_multi_count_tb;

reg refclk;
integer cc;
wire [27:0] frequency;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("freq_multi_count.vcd");
		$dumpvars(5,freq_multi_count_tb);
	end
	for (cc=0; cc<1000; cc=cc+1) begin
		refclk=0; #10;
		refclk=1; #10;
	end
	// Simulated accumulation interval is 20*64 = 1280 ns
	// Should catch an average of 1280/6 = 213.33 clk edges in that time
	if (frequency > 195 && frequency < 201) $display("PASS");
	else $display("FAIL");
	$finish();
end

reg [3:0] uclk=0;
always begin
	uclk[0]=0; #3;
	uclk[0]=1; #3;
end

wire [4:0] source_state;
freq_multi_count #(.NF(4), .NA(2), .rw(6)) mut(.unk_clk(uclk), .refclk(refclk),
	.addr(2'd0), .source_state(source_state), .frequency(frequency)
);

endmodule
