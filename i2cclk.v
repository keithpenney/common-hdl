module i2cclk(input clk
,input [31:0] clk4ratio
,output [1:0] state
,input start
,input stop
,output tick0
,output tick1
,output tickp
,output [31:0] dbclkcnt
);

reg [31:0] clkcnt=32'hffffffff;
wire clkcnt0=~|clkcnt;
reg [1:0] state_r=0;
reg [1:0] state_r2=0;
reg run=0;
reg tick_r=0;
reg tick_r2=0;
reg tick_pr=0;
always @(posedge clk) begin
	clkcnt<=(start | clkcnt0)? clk4ratio : clkcnt-run;
	state_r<=start ? 0 : state_r+clkcnt0;
	state_r2<=state_r;
	if(start) begin
		run<=1'b1;
	end
	else  if (stop) begin
		run<=1'b0;
	end
	tick_r<=clkcnt0;
	tick_r2<=tick_r;
	tick_pr<=tick_r2;
end
assign state=state_r2;
assign tick1=tick_r;
assign tick0=clkcnt=={clk4ratio[31],clk4ratio[31:1]};
assign tickp=tick_pr;
assign dbclkcnt=clkcnt;
endmodule
