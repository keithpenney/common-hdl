module i2cslave#(parameter DEVADDR=8'h62,parameter ACKWIDTH=8)
(input clk
,input SCL
//,inout SDA
,input slavesdarx
,output slavesdatx
,output slavesdaasrx
,output [ACKWIDTH-1:0] datarx
,input [ACKWIDTH-1:0] datatx
,input rst
,output rxvalid
,output [ACKWIDTH-1:0] ack1
,output ack1valid
,input [15:0] nack
,output [7:0] addr
,output w0r1
);
//wire slavesdatx;
//wire slavesdarx;
reg slavesdaasrx_r=1'b1;
assign slavesdaasrx=slavesdaasrx_r;
//assign SDA= slavesdaastx ? slavesdatx : 1'bz; // line or
//assign slavesdarx= SDA;
//assign SDA=slavesdaasrx ? 1'bz : slavesdatx;
//assign slavesdarx = slavesdaasrx ? SDA : 1'b1;

localparam IDLE =3'd0;
localparam START=3'd1;
localparam DEV  =3'd2;
localparam DATA =3'd3;
localparam ACK  =3'd4;
localparam STOP =3'd5;
reg [15:0] test=0;
reg run=0;
reg scl_d=0;
reg slavesdarx_d=0;
wire m0=slavesdarx==1'b0;
wire m1=slavesdarx==1'b1;
wire mx=slavesdarx==1'bx;
always @(posedge clk) begin
	scl_d<=SCL;
	slavesdarx_d<=slavesdarx;
end

wire scl=SCL;
wire start= (scl&scl_d&slavesdarx_d&~slavesdarx);
wire stop=scl&scl_d&slavesdarx&~slavesdarx_d;
wire sclfaling=~scl&scl_d;
wire sclrising=scl&~scl_d;

reg low=0;
reg high=0;
wire samprx=high&(highcnt==(lowcnt>>1))&|lowcnt&|highcnt;
always @(posedge clk) begin
	if (sclfaling) begin
		low<=1'b1;
		high<=1'b0;
	end else if (sclrising) begin
		low<=1'b0;
		high<=1'b1;
	end
end
reg [ACKWIDTH:0] slavedatarxsr=0;
reg [ACKWIDTH:0] slavedatatxsr=0;
reg [ACKWIDTH:0] testsr=0;
reg [ACKWIDTH-1:0] ack1r=0;
reg ack1validr=0;
assign ack1=ack1r;
assign ack1valid=ack1validr;
reg [2:0] slavestate=IDLE;
reg [2:0] slavenext=IDLE;
reg [15:0] lowcnt=0;
reg [15:0] highcnt=0;
reg valid_r=0;
reg [ACKWIDTH-1:0] slavedatarx_r=0;
reg slavedatarxvalid_r=0;
always @(posedge clk) begin
	if (rst) begin
		slavestate<=IDLE;
	end
	else begin
		slavestate<=slavenext;
	end
	if (run) begin
		lowcnt<=sclfaling ? 0 : lowcnt+low;
		highcnt<=sclrising ? 0 : highcnt+high;
	end
	else begin
		lowcnt<=0;
		highcnt<=0;
	end
end
reg [15:0] slavedatacnt=0;
reg [15:0] ackcnt=0;
wire toack =(sclfaling & slavedatacnt==ACKWIDTH);
reg toack_d=0;
reg [7:0] devaddr=0;
reg devw0r1=0;
wire devmatch=devaddr==DEVADDR;
always @(*) begin
	case (slavestate)
		IDLE:	slavenext= start ? START : IDLE;
		START:	slavenext= sclfaling ? DEV: START;
		DEV: 	slavenext = stop ? STOP : start ? START: toack_d ? ACK : DEV;
		DATA: 	slavenext=	stop ? STOP : start ? START: toack_d ? ACK : DATA;
		ACK:  	slavenext=  (sclfaling & ackdone) ?( devmatch ? DATA : IDLE) :ACK;
		STOP: 	slavenext=IDLE;
		default: 	slavenext=IDLE;
	endcase
end
wire acktx=0;
reg acktx_r=0;
reg ackdone=0;
reg [ACKWIDTH:0] devaddrsr=0;
reg slavesdatx_r=0;
reg ackrx_r=0;
reg devaddrvalid=0;
always @(posedge clk) begin
	toack_d<=toack;
	if (rst) begin
		slavedatatxsr<=0;
		slavedatarxsr<=0;
		slavesdaasrx_r<=1'b1;
	end
	else begin
		case (slavenext)
			IDLE: begin
				slavedatatxsr<=0;
				slavedatarxsr<=0;
				devaddrsr<=0;
				slavedatacnt<=0;
				slavesdaasrx_r<=1'b1;
				ackdone<=1'b0;
				ackcnt<=0;
				ack1validr<=1'b0;
				devaddrvalid<=1'b0;
			end
			START: begin
				slavedatatxsr<={1'b0,datatx};
				slavedatacnt<=0;
				devaddrsr<=0;
				slavesdaasrx_r<=1'b1;
				ackdone<=1'b0;
				ack1validr<=1'b0;
				//ackcnt<=0;
				run<=1'b1;
				devaddrvalid<=1'b0;
			end
			DEV: begin
				slavesdaasrx_r<=1'b1;
				if (samprx) begin
					slavedatacnt<=slavedatacnt+1'b1;
					devaddrsr<={devaddrsr[ACKWIDTH-1:0],slavesdarx};
				end
				if (toack) begin
					{devaddr,devw0r1}<=devaddrsr[ACKWIDTH-1:0];
					devaddrvalid<=1'b1;
				end
				else begin
					devaddrvalid<=1'b0;
				end
			end
			DATA: begin
				ackdone<=1'b0;
				slavesdaasrx_r<=devw0r1 ? 1'b0 : 1'b1;
				slavesdatx_r<=slavedatatxsr[ACKWIDTH];
				if (devw0r1) begin
					if (sclfaling) begin
						slavedatacnt<=slavedatacnt+1'b1;
						slavedatatxsr<={slavedatatxsr[ACKWIDTH-1:0],1'b0};
					end
				end
				else begin
					if (samprx) begin
						slavedatacnt<=slavedatacnt+1'b1;
						slavedatarxsr<={slavedatarxsr[ACKWIDTH-1:0],slavesdarx};
					end
				end
				if (toack) begin
					slavedatarx_r<=slavedatarxsr[ACKWIDTH-1:0];
					slavedatarxvalid_r<=1'b1;
				end
				else begin
					slavedatarxvalid_r<=1'b0;
				end
			end
			ACK:  begin
				test<=test+1;
				slavesdaasrx_r <= devw0r1 ? 1'b1 : 1'b0;
				if (samprx) begin
					ackdone<=1'b1;
				end
				if (devw0r1) begin
					slavesdatx_r<=acktx;
				end
				else begin
						ackrx_r<=slavesdarx;
				end
				if (slavestate!=ACK) begin
					ackcnt<=ackcnt+1;
				end
				slavedatacnt<=0;
			end
			STOP: begin
				devaddrvalid<=1'b0;
				ack1validr<=1'b0;
				ackdone<=1'b0;
				slavesdaasrx_r<=1'b1;
				run<=1'b0;
				ackcnt<=0;
			end
			default: begin
				slavesdaasrx_r<=1'b1;
			end
		endcase
	end
	valid_r<=stop;
end
assign datarx=slavedatarx_r;
assign rxvalid=valid_r;
assign slavesdatx=slavesdatx_r;//kkkkkslavestate==slavesdatx_rACK ?  acktx :slavedatatxsr[ACKWIDTH];
endmodule
/*reg [6:0] devaddr_r=0;
reg [7:0] addr_r=0;
reg w0r1_r=0;
assign {devaddr,w0r1,addr}={devaddr_r,w0r1_r,addr_r};
*/
