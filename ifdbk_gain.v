`timescale 1ns / 1ns

// Approximate resource footprint in Spartan-6: 4 DSP48A1, 392 slice LUTs.
// Or maybe 326 LUTs, if you use XST 14.7.
module ifdbk_gain(
	input clk,  // timespec 7.9 ns
	input zerome,
	input [35:0] s0,  // stream of double-integrated data from cim_12
	input svalid,

	// Local bus
/*
	input [31:0] lb_data,
	input [6:0] lb_addr,
	input lb_write,
*/
	// Digital LO for upconversion
	input signed [17:0] sind,
	input signed [17:0] cosd,

	// Output
	output signed [15:0] d_out  // at IF

,output reg [1:0] radd
,input signed [17:0] xy_ctl
);

parameter dw = 36;  // width of s0

// Host-settable registers
//`include "regmap_app.vh"
// Our only registers are an array of four, not supported by the automatic chain.
// Search below for lb_addr usage.

// Universal definition; note: old and new are msb numbers, not bit widths.
`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})

// Two stages of differentiator
// Kind of stupid to do this before channel selection; no resource
// penalty on Xilinx, though.
wire valid2;
wire [dw-1:0] d2;
doublediff #(.dw(dw), .dsr_len(12)) diff(.clk(clk),
	.d_in(s0), .g_in(svalid), .d_out(d2), .g_out(valid2));

// Local timing; cavity I and Q values are in time slots 7 and 8 out of 12.
reg [3:0] lc=0;
//reg [1:0] radd=0;
reg mul_gate=0, mul_iq=0, snap=0;
always @(posedge clk) begin
	lc <= valid2 ? lc+1'b1 : 1'b0;
	radd <= lc[1:0];
	mul_gate <= lc==7||lc==8;  // not used in hardware, but nice guide to the eye in simulations
	mul_iq <= lc[0];
	snap <= lc==12;
end

// Select bits based on known (ha!) static timing of 22 cycles per sample
// 22^2 = 484, so shift 9 to divide by 512.
// Well, multiply that by a factor of 2 to correct for mean(sin^2).
// Keep an "extra" high bit to be resilient when ADC has clipped.
wire signed [18:0] xy_val = d2[26:8];

// DPRAM to hold X set, Y set, X gain, Y gain
//wire signed [17:0] xy_ctl;
// lb_addr mapping matches full decimal addresses 56, 57, 58, and 59.
/*wire dpram_write = lb_write & (lb_addr[6:2]==14);
dpram #(.dw(18), .aw(2)) hbuf(.clka(clk), .clkb(clk),
	.addra(lb_addr[1:0]), .dina(lb_data[17:0]), .wena(dpram_write),
	.addrb(radd), .doutb(xy_ctl));
*/
// Subtract setpoint
reg signed [19:0] xy_err=0;
reg signed [17:0] xy_err_h=0;
//wire signed [17:0] xy_val = d3;
always @(posedge clk) begin
	xy_err <= xy_val - xy_ctl;
	xy_err_h <= `SAT(xy_err,19,17);
end

// Test-bench only, create always-valid registers that can be probed
`ifdef SIMULATE
reg signed [17:0] x_err=0, y_err=0;
reg mul_gate_d=0;
always @(posedge clk) begin
	mul_gate_d <= mul_gate;
	if (mul_gate & ~mul_gate_d) x_err <= xy_err;
	if (mul_gate &  mul_gate_d) y_err <= xy_err;
end
`endif

// Complex multiply
wire signed [17:0] mul_result;
wire mul_gate_out;
vectormul gain(.clk(clk), .gate_in(mul_gate), .iq(mul_iq),
	.x(xy_err_h), .y(xy_ctl), .z(mul_result), .gate_out(mul_gate_out));

// Snapshot
reg signed [17:0] mul_result_d=0, snap_i=0, snap_q=0;
always @(posedge clk) begin
	mul_result_d <= mul_result;
	if (snap) snap_i <= mul_result_d;
	if (snap) snap_q <= mul_result;
end

// Integrate
// Wish to support system unity gain at 30 kHz even when the
// system gain is 1/10.
// Thus design for maximum unity gain-bandwidth of 300 kHz,
// or 2 Mrad/sec.  At 100 MHz clocks, that's 1/50 (round to 1/64)
// We have 18 bits in, 17 bits out.
reg signed [23:0] int_i=0, int_q=0;
wire signed [24:0] int_ii = int_i + snap_i;
wire signed [24:0] int_qq = int_q + snap_q;
always @(posedge clk) begin
	int_i <= zerome ? 0 : `SAT(int_ii,24,23);
	int_q <= zerome ? 0 : `SAT(int_qq,24,23);
end
// Observation: for stability at 30 kHz, want time delay less than
// 1/10f = 3 us or 300 clock cycles.  The order 2 N=22 CIC filter at
// the input of this signal path contributes an essential 22 cycles delay.
// The pipelining stages in this module (from xy_in to d_out) total 12 cycles.
// Looking at the simulation I see about 52 cycles delay total.

// Upconvert and sum
flevel_set upconv(.clk(clk), .cosd(cosd), .sind(sind),
	.i_data(int_i[23:7]), .i_gate(1'b1), .i_trig(1'b0),
	.q_data(int_q[23:7]), .q_gate(1'b1), .q_trig(1'b0),
	.o_data(d_out)
);

endmodule
