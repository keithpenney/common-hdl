`timescale 1ns / 1ns

// Input DC-reject filter, suited for carriers near f_sample*2/11,
// e.g., APEX with 102.14 MHz clock.
// Zero-multiplier footprint
module infilt(
	input clk,  // timespec 8.0 ns
	input signed [15:0] d_in,
	output signed [15:0] d_out
);

// Universal definition; note: old and new are msb numbers, not bit widths.
`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})

// z-transform filter gain
//  0.5*(1-0.5*(z.^(-2)+z.^(-3)))
// see octave/infilt_185.m
reg signed [15:0] d1=0, d2=0, d3=0;  // input z^(-1) registers
reg signed [16:0] s1=0; // sum registers
reg signed [17:0] s2=0;
wire signed [17:0] sum = $signed({d_in,1'b1}) - s1;  // one lsb guard, with rounding

always @(posedge clk) begin
	d1 <= d_in;
	d2 <= d1;
	s1 <= d1 + d2;
	s2 <= sum;
end
assign d_out = s2[17:2];

endmodule
