interface ifddr3();
wire reset_b;
wire clk1_p,clk1_n,clk0_p,clk0_n,cke0,cke1;
wire we_b,ras_b,cas_b,odt0,odt1;
wire temp_event;
wire [63:0] d;
wire [7:0] dqs_p,dqs_n;
wire [7:0] dm;
wire [15:0] a;
wire [2:0] ba;
wire s0_b,s1_b;
modport hw(
inout d,dqs_p,dqs_n
,input a
,input ba
,input cke0,cke1
,input clk0_n,clk0_p,clk1_n,clk1_p
,input dm
,input odt0,odt1
,input ras_b,cas_b,we_b
,input reset_b
,input s0_b,s1_b
,output temp_event
);
endinterface
module ddr3_hw(ifddr3.hw ddr3
,inout [63:0] d
,inout [7:0] dqs_p,dqs_n
,output [15:0] a
,output [2:0] ba
,output [7:0]dm
,output cke0,cke1
,output clk0_n,clk0_p,clk1_n,clk1_p
,output odt0,odt1
,output ras_b,cas_b,we_b
,output reset_b
,output s0_b,s1_b
,input temp_event
);
assign a=ddr3.a;
assign ba=ddr3.ba;
assign dm=ddr3.dm;
assign {cke0,cke1,clk0_n,clk0_p,clk1_n,clk1_p,odt0,odt1,ras_b,cas_b,we_b,reset_b,s0_b,s1_b}={ddr3.cke0,ddr3.cke1,ddr3.clk0_n,ddr3.clk0_p,ddr3.clk1_n,ddr3.clk1_p,ddr3.odt0,ddr3.odt1,ddr3.ras_b,ddr3.cas_b,ddr3.we_b,ddr3.reset_b,ddr3.s0_b,ddr3.s1_b};

endmodule
