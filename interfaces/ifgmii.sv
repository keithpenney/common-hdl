interface ifgmii();
	wire tx_clk;
	wire rx_clk;
	wire [7:0] txd;
	wire tx_en;
	wire tx_er;
	wire [7:0] rxd;
	wire rx_dv;
	wire rx_er;
	wire phy_rst_n;
	modport clks(
		output tx_clk, rx_clk
	);
	modport driver(  // intended for attachment to logic
		input tx_clk, rx_clk,
		output txd, tx_en, tx_er, phy_rst_n,
		input rxd, rx_dv, rx_er
	);
	modport hw(  // intended for attachment to hardware, e.g., GMII pins, RMGII, or GTX
		input tx_clk, rx_clk,
		input txd, tx_en, tx_er, phy_rst_n,
		output rxd, rx_dv, rx_er
	);

	// Either use the clks modport or use the phy to set tx/rx clks
	modport phy(
		output tx_clk, rx_clk,
		input txd, tx_en, tx_er,
		output rxd, rx_dv, rx_er
	);
	modport eth(  // same as driver except no phy_rst_n
		input tx_clk, rx_clk,
		output txd, tx_en, tx_er,
		input rxd, rx_dv, rx_er
	);
endinterface

module gmii_setclk(ifgmii.clks gmii
	,input tx_clk
	,input rx_clk
);
assign {gmii.tx_clk,gmii.rx_clk}={tx_clk,rx_clk};
endmodule

// Provide (non-System-)Verilog signals for (possibly virtual) PHY attachment
module gmii_phy(ifmgii.hw gmii
	,output tx_clk, rx_clk
	,output txd, tx_en, tx_er, phy_rst_n
	,input rxd, rx_dv, rx_er
);
assign {txd,tx_en,phy_rst_n,tx_er,tx_clk,rx_clk}={gmii.txd,gmii.tx_en,gmii.phy_rst_n,gmii.tx_er,gmii.tx_clk,gmii.rx_clk};
assign {gmii.rx_dv,gmii.rxd,gmii.rx_er}={rx_dv,rxd,rx_er};
endmodule
