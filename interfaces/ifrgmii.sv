interface ifrgmii();
	wire [3:0] txd,rxd;
	wire phy_rst_n;
	wire rx_dv;
	wire rx_clk;
	wire tx_en;
	wire tx_clk;
modport hw(
	input txd,tx_clk,rx_clk,tx_en,phy_rst_n
	,output rx_dv,rxd
	);
modport phyrst(output phy_rst_n);
endinterface

