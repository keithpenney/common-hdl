`timescale 1ns / 1ns
module lo2(input clk
,input [PHASE1WIDTH-1:0] phase_h
,input [PHASE1WIDTH-1:0] phase_offset
,input [(NRIDER+1)*WIDTHP1-1:0] xlo
,input [(NRIDER+1)*WIDTHP1-1:0] ylo
,output signed [(NRIDER+1)*WIDTH-1:0] cos
,output signed [(NRIDER+1)*WIDTH-1:0] sin
);
parameter NRIDER=0;
parameter WIDTH=20;
localparam ZWIDTH=WIDTH+1;
localparam ZWIDTHP1=ZWIDTH+1;
parameter PHASE1WIDTH=31;
parameter PHSTEPH=16268815;
parameter PHSTEPL=2108;
parameter MODULO=4;
parameter NSTAGE=WIDTH;
localparam WIDTHP1=WIDTH+1;

wire [0:0] errorlo;
reg [PHASE1WIDTH-1:0] phase_hr=0;
reg [ZWIDTHP1-1:0] phaselobase=0;
reg [ZWIDTHP1-1:0] phaselobase_d=0;
always @(posedge clk) begin
	phase_hr<=phase_h+phase_offset;
	phaselobase <= phase_hr[PHASE1WIDTH-1:PHASE1WIDTH-ZWIDTHP1];
//	phaselobase <= phase_hr[ZWIDTHP1-1:0];
	phaselobase_d<=phaselobase;
end
wire [ZWIDTHP1-1:0] deltaphase=phaselobase-phaselobase_d;

wire [(NRIDER+1)*ZWIDTHP1-1:0] phasein={(NRIDER+1){phaselobase}};//+phsteph_offset_use}};
wire [(NRIDER+1)*(WIDTH+1)-1:0] xout,yout;
wire [(NRIDER+1)*(ZWIDTH+1)-1:0] zout;
wire cordicgerror;
cordicg1 #(.WIDTH(WIDTHP1),.NSTAGE(NSTAGE),.NORMALIZE(0),.NRIDER(NRIDER),.BUFIN(1'b1))
cordicg1(.clk(clk),.opin(1'b0),.phasein(phasein),.xin(xlo),.yin(ylo)//{WIDTHP1{1'b0}})
,.xout(xout),.yout(yout),.phaseout(zout)
,.error(cordicgerror)
);
genvar irider;
generate for (irider=0; irider<NRIDER+1; irider=irider+1) begin: lo
assign cos[(irider+1)*WIDTH-1:irider*WIDTH]=xout[(irider+1)*(WIDTH+1)-1:irider*(WIDTH+1)+1]+xout[irider*(WIDTH+1)];
assign sin[(irider+1)*WIDTH-1:irider*WIDTH]=yout[(irider+1)*(WIDTH+1)-1:irider*(WIDTH+1)+1]+yout[irider*(WIDTH+1)];
end
endgenerate
assign errorlo=zout[ZWIDTH-1:1]|(~|zout[ZWIDTH-1:1])|cordicgerror;
assign error = |errorlo;
endmodule
