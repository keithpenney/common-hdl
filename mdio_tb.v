`timescale 1ns / 1ns
module mdio_tb();
reg sysclk=0;
integer cc=0;
initial begin
	$dumpfile("mdio.vcd");
	$dumpvars(17,mdio_tb);
	for (cc=0; cc<1000000; cc=cc+1) begin
//while (1) begin
//		cc=cc+1;
		sysclk=0; #2.5;
		sysclk=1; #2.5;
	end
	$finish();
end

reg [31:0] clkcnt=0;
always @(posedge sysclk) begin
	clkcnt<=clkcnt+1;
end
wire start=clkcnt==24;

wire [31:0] clk4ratio=20;
wire [31:0] datarx;
wire [15:0] datatx=16'h2281;
wire  mdc;
wire  mdio_o;
wire  mdio_t;
wire  mdio_i=mdio_t ? 1'b1 : mdio_o;
wire  opr1w0=1'b0;
wire [4:0] phyaddr=5'b00111;
wire [4:0] regaddr=5'h1c;
wire  rst=1'b0;
mdiomasterinit #(.INITLENGTH(4),.INITWIDTH(27),.INITCLK4RATIO(100),.INITCMDS(
	{1'b0,5'b00111,5'h0,16'h0140
	,1'b0,5'b00111,5'h4,16'h9801
	,1'b0,5'b00111,5'h16,16'h1
	,1'b0,5'b00111,5'h0,16'h8140
	}
))
mdiomaster(.clk(sysclk),.busy(busy),.clk4ratio(clk4ratio),.datarx(datarx),.datatx(datatx),.mdc(mdc),.mdio_i(mdio_i),.mdio_o(mdio_o),.mdio_t(mdio_t),.opr1w0(opr1w0),.phyaddr(phyaddr),.regaddr(regaddr),.rst(rst),.rxvalid(rxvalid),.start(start));
endmodule
