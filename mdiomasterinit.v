module mdiomasterinit
(input clk
,input [31:0] clk4ratio
,input mdio_i
,output mdio_o
,output mdio_t
,output mdc
,input rst
,input start
,input opr1w0
,input [4:0] phyaddr
,input [4:0] regaddr
,input [15:0] datatx
,output [31:0] datarx
,output rxvalid
,output busy
,output initdone
,output [3:0] dbinitstate
,output [3:0] dbinitnext
);
reg initstart=0;
reg start_i=0;
wire opr1w0_i;
wire [4:0] phyaddr_i;
wire [4:0] regaddr_i;
wire [15:0] datatx_i;
reg [31:0] clk4ratio_i=0;
parameter INITWIDTH=27;
parameter INITLENGTH=4;
parameter INITCMDS=0;
parameter INITCLK4RATIO=100000;
reg init=1;
//reg reset=0;
reg [15:0] initcnt=INITLENGTH;
localparam INITIDLE=4'd5;
localparam INITPREP=4'd0;
localparam INITSTART=4'd1;
localparam INITRUN=4'd2;
localparam INITNEXT=4'd3;
localparam INITDONE=4'd4;
reg done=0;
reg [3:0] initstate=INITIDLE;
reg [3:0] initnext=INITIDLE;
always @(posedge clk) begin
	if (rst)
		initstate<=INITIDLE;
	else begin
		initstate<=initnext;
	end
	initstart<=rst;
end
always @(*) begin
	case (initstate)
		INITIDLE: initnext = initstart ? INITPREP : INITIDLE;
		INITPREP: initnext = INITSTART ;
		INITSTART: initnext = busy ? INITRUN : INITSTART;
		INITRUN: initnext = busy ? INITRUN : INITNEXT;
		INITNEXT: initnext= |initcnt ? INITPREP: INITDONE;
		INITDONE: initnext =INITDONE;
	endcase
end
reg [INITWIDTH-1:0] initcmd=0;
assign {opr1w0_i,phyaddr_i,regaddr_i,datatx_i}=initcmd;
always @(posedge clk) begin
	//reset<=rst;
	if (rst) begin
		initcnt<=INITLENGTH;
		clk4ratio_i<=INITCLK4RATIO;
		initcmd<=0;
		start_i<=1'b0;
		done<=1'b0;
	end
	else begin
		case (initnext)
			INITIDLE,INITPREP: begin
				clk4ratio_i<=INITCLK4RATIO;
				initcmd<=INITCMDS[(INITLENGTH-initcnt)*INITWIDTH+:INITWIDTH];
				start_i<=1'b0;
				done<=1'b0;
			end
			INITSTART: begin
				start_i<=1'b1;
			end
			INITRUN: begin
				start_i<=1'b0;
			end
			INITNEXT: begin
				initcnt<=initcnt-1;
			end
			INITDONE: begin
				clk4ratio_i<=clk4ratio;
				initcmd<={opr1w0,phyaddr,regaddr,datatx};
				start_i<=start;
				done<=1'b1;
			end
		endcase
	end
end
assign initdone=done;
mdiomaster mdiomaster
(.clk(clk)
,.clk4ratio(clk4ratio_i)
,.mdio_i(mdio_i)
,.mdio_o(mdio_o)
,.mdio_t(mdio_t)
,.mdc(mdc)
,.rst(rst)
,.start(start_i)
,.phyaddr(phyaddr_i)
,.regaddr(regaddr_i)
,.opr1w0(opr1w0_i)
,.datatx(datatx_i)
,.datarx(datarx)
,.rxvalid(rxvalid)
,.busy(busy));

endmodule
