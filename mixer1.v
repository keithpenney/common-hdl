`timescale 1ns / 1ns

module mixer(clk,adcf,mult,mixout);
parameter dwi=16;  // data width
parameter davr=4;  // average width if the following cic averaging 64,
// then the useful data increase sqrt(64), so increase 3 bits,
// for rounding error, save 4 bits more
parameter dwlo=18;  // lo width using 18 bits
input clk;  // timespec 8.4 ns
(* mark_debug = "true" *)input signed [dwi-1:0] adcf;  // possibly muxed
(* mark_debug = "true" *)input signed [dwlo-1:0] mult;
(* mark_debug = "true" *)output signed [dwi-1+davr+MSBGUARD+1:0] mixout;

(* mark_debug = "true" *)reg signed [dwi-1+davr+MSBGUARD+1:0] mixout_r=0;
(* mark_debug = "true" *)reg signed [dwi-1:0] adcf1=0;
(* mark_debug = "true" *)reg signed [dwlo-1:0] mult1=0;
//reg signed [dwi+dwlo-1:0] mix_out_r=0;
(* mark_debug = "true" *)reg signed [dwi-1+davr+MSBGUARD:0] mix_out1=0, mix_out2=0;
(* mark_debug = "true" *)wire signed [dwi+dwlo-1+MSBGUARD:0] mix_out_w=adcf1*mult1;
always @(posedge clk) begin
		adcf1 <= adcf;
		mult1 <= mult;
end
localparam MSBGUARD=(NORMALIZE==2)? 1 :0;
parameter NORMALIZE=0;
generate
if (NORMALIZE==1) begin
	// mixer have a 1/2 gain, 2cos(a)cos(b)=(cos(a+b)+cos(a-b)) ?? wrong way?
	always @(posedge clk) begin
		mixout_r <= mix_out_w[dwi+dwlo-1:dwlo-davr-1]+mix_out_w[dwlo-davr-2];
	end
end
else if (NORMALIZE==0)begin
	always @(posedge clk) begin
		mixout_r <={mix_out_w[dwi+dwlo-1],mix_out_w[dwi+dwlo-1:dwlo-davr]+mix_out_w[dwlo-davr-1]};
	end
end
else if (NORMALIZE==2)begin
	always @(posedge clk) begin
		mixout_r <=mix_out_w>>>(dwlo-davr-1-1);// [dwi+dwlo-2+MSBGUARD:dwlo-davr-2+MSBGUARD]+mix_out_w[dwlo-davr-3+MSBGUARD];
	end
end
endgenerate
assign mixout = mixout_r;
endmodule
