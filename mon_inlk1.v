module mon_inlk1(input clk
,input signed [WIDTH-1:0] amp
,output cmph_status
,output cmpl_status
,output trip
,input [1:0] tripmode
,input [WIDTH-2:0] thresh1
,input [WIDTH-2:0] thresh2
);
parameter WIDTH=16;
wire [WIDTH-1:0] threshh={1'b0,(thresh1>=thresh2)?thresh1:thresh2};
wire [WIDTH-1:0] threshl={1'b0,(thresh1>=thresh2)?thresh2:thresh1};
reg cmpgh=0;
reg cmpgl=0;
reg trip_r=0;
always @(posedge clk) begin
	cmpgh<= amp >= threshh;
	cmpgl<= amp >= threshl;
	trip_r <= (tripmode=={cmpgh,cmpgl}) | ((tripmode==2'b10)&(cmpgh|~cmpgl));
end
// cmph/l status valid 1 cycle after the data
// trip valid 2 cycle after the data
assign cmph_status=cmpgh;
assign cmpl_status=cmpgl;
assign trip=trip_r;

endmodule
