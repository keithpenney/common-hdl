module mon_inlk2(input clk
,input [WIDTH-1:0] amp
,input gate
,input strobe
,output [2:0] a_thresh
,input [31:0] v_thresh
,output [NCHAN-1:0] cmph_status
,output [NCHAN-1:0] cmpl_status
,output [NCHAN-1:0] trip
,output [NCHAN-1:0] trip_latched
,input [0:0] reset
);
parameter NCHAN=8;
parameter WIDTH=16;
parameter WADDR=clog2(NCHAN);
function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction
reg [WADDR-1:0] chancnt=0;
reg gate_d1=0;
reg gate_d2=0;
reg gate_d3=0;
reg strobe_d1=0;
reg strobe_d2=0;
reg strobe_d3=0;
reg [WIDTH-1:0] amp_d=0;
reg [1:0] tripmode=0;//{v_thresh[31],v_thresh[15]};
reg [14:0] thresh1=0;//v_thresh[30:16];
reg [14:0] thresh2=0;//v_thresh[14:0];
always @(posedge clk) begin
	amp_d<=amp;
	gate_d1<=gate;
	gate_d2<=gate_d1;
	gate_d3<=gate_d3;
	strobe_d1<=strobe;
	strobe_d2<=strobe_d1;
	strobe_d3<=strobe_d2;
	chancnt <= ~gate ? 0 : strobe ? chancnt+1 : chancnt;
	tripmode<={v_thresh[31],v_thresh[15]};
	thresh1<=v_thresh[30:16];
	thresh2<=v_thresh[14:0];
end
assign a_thresh=chancnt;

wire cmph_status_chan;
wire cmpl_status_chan;
wire trip_chan;
reg trip_chan_r=0;
wire trip_latched_chan;
mon_inlk1 mon_inlk1(.clk(clk)
,.amp(amp)
,.tripmode(tripmode)
,.thresh1(thresh1)
,.thresh2(thresh2)
,.cmph_status(cmph_status_chan)
,.cmpl_status(cmpl_status_chan)
,.trip(trip_chan)
);
reg [NCHAN-1:0] cmph_status_sr=0;
reg [NCHAN-1:0] cmpl_status_sr=0;
reg [NCHAN-1:0] trip_sr=0;
reg [NCHAN-1:0] trip_latched_sr=0;
reg [NCHAN-1:0] cmph_status_r=0;
reg [NCHAN-1:0] cmpl_status_r=0;
reg [NCHAN-1:0] trip_r=0;
reg [NCHAN-1:0] trip_latched_r=0;
wire first=gate&~gate_d1;
reg first_d1=0;
wire last=~gate&gate_d1;
reg last_d1=0;
reg last_d2=0;
always @(posedge clk) begin
	first_d1<=first;
	last_d1<=last;
	last_d2<=last_d1;
	trip_chan_r<=trip_chan;
	if (gate_d1&strobe_d1) begin
		cmph_status_sr<=(first ? {cmph_status_chan,{NCHAN-2{1'b0}}} : {cmph_status_chan,cmph_status_sr[NCHAN-1:1]});
		cmpl_status_sr<=(first ? {cmpl_status_chan,{NCHAN-2{1'b0}}} : {cmpl_status_chan,cmpl_status_sr[NCHAN-1:1]});
	end
	if (gate_d2&strobe_d2) begin
		trip_sr<=(first ? {trip_chan,{NCHAN-2{1'b0}}} : {trip_chan,trip_sr[NCHAN-1:1]});
	end
	if (last_d2) begin
//	if (last) begin //test in hires
		cmph_status_r<=cmph_status_sr;
		cmpl_status_r<=cmpl_status_sr;
		trip_r<=trip_sr;
	end
	trip_latched_r <= reset ? 0 : (trip_latched_r | trip_r);
end
assign cmph_status=cmph_status_r;
assign cmpl_status=cmpl_status_r;
assign trip=trip_r;
assign trip_latched=trip_latched_r;
endmodule
