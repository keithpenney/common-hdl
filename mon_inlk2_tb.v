`timescale 1ns/10ps
module mon_inlk2_tb;
reg dspclk;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("mon_inlk2.vcd");
		$dumpvars(17,mon_inlk2_tb);
	end
	for (cc=0;cc<300; cc=cc+1) begin
		dspclk=0; #5.2;
		dspclk=1; #5.2;
	end
	$finish();
end

reg [31:0] mem[7:0];
integer k=0;
initial
begin
	for (k=0;k<8;k=k+1)
	begin
		mem[k]=32'h33338333;//(8-k)*(32'h11111111);
	end
end
wire [31:0] mem0=mem[0];
wire [31:0] mem1=mem[1];
wire [31:0] mem2=mem[2];
wire [31:0] mem3=mem[3];
wire [31:0] mem4=mem[4];
wire [31:0] mem5=mem[5];
wire [31:0] mem6=mem[6];
wire [31:0] mem7=mem[7];

reg [15:0] clkcnt=0;
reg [31:0] ampr=0;
always @(posedge dspclk) begin
	clkcnt<=clkcnt+1;
	if (clkcnt[4]) begin
		case (clkcnt[3:1])
			3'd7: ampr<=16'h2222;
			3'd1: ampr<=16'h1111;
			3'd2: ampr<=(cc>16'h40 & cc<16'h70)? 16'h2000 : 16'h0000;
			/*
			3'd2: ampr<=16'h2222;
			3'd3: ampr<=16'h3333;
			3'd4: ampr<=16'h4444;
			3'd5: ampr<=16'h5555;
			3'd6: ampr<=16'h6666;
			3'd7: ampr<=16'h7777;
			*/
			default: ampr<=16'h0000;
		endcase
	end
end
localparam WIDTH=16;
localparam NCHAN=8;
wire [2:0] a_thresh;
wire [WIDTH-1:0] amp=ampr;
wire  clk=dspclk;
wire [NCHAN-1:0] cmph_status;
wire [NCHAN-1:0] cmpl_status;
wire  gate=clkcnt[4];
wire [0:0] reset=clkcnt==16'hc0;
wire  strobe=clkcnt[0];
wire [NCHAN-1:0] trip;
wire [NCHAN-1:0] trip_latched;
wire [31:0] v_thresh=mem[a_thresh];
mon_inlk2 mon_inlk2(.a_thresh(a_thresh)
,.amp(amp)
,.clk(clk)
,.cmph_status(cmph_status)
,.cmpl_status(cmpl_status)
,.gate(gate)
,.reset(reset)
,.strobe(strobe)
,.trip(trip)
,.trip_latched(trip_latched)
,.v_thresh(v_thresh));
endmodule
