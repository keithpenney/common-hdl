`timescale 1ns / 1ns
module mult_trad1(input clk
,input [WX-1:0] x
,input [WY-1:0] y
,output [WX+WY-1:0] z
);
parameter WX=9;
parameter WY=30;
parameter SIGNED=0;

localparam WABSX=SIGNED ? WX-1 : WX;
localparam WABSY=SIGNED ? WY-1 : WY;

reg [WX-1:0] ssr =0;
reg [WX*WABSX-1:0] xsr =0;
reg [WY*WABSX-1:0] ysr =0;
reg [(WABSX+WABSY)*(WABSX+1)-1:0] sumsr=0;
always @(posedge clk) begin
	xsr<={xsr[WX*WABSX-WX-1:0],x};
	ysr<={ysr[WY*WABSX-WY-1:0],y};
end

genvar ix;
generate
	for (ix=0;ix<WABSX;ix=ix+1) begin
		wire [WABSX-1:0] xi=xsr[ix*WX+WABSX-1:(ix*WX)];
		wire [WABSY-1:0] yi=ysr[ix*WY+WABSY-1:(ix*WY)];
		wire [WABSX+WABSY-1:0] yishift = (yi<<ix);
		reg [WABSX+WABSY-1:0] sumsri_1r=0;
		wire [WABSX+WABSY-1:0] sumsri=sumsr[(ix+2)*(WX+WY)-1:(ix+1)*(WX+WY)];
		always @(posedge clk) begin
			sumsr[(ix+2)*(WX+WY)-1:(ix+1)*(WX+WY)] <= xi[ix] ? (sumsri+yishift) : (sumsri-yishift);
		end
	end
endgenerate
reg sign=0;
always @(posedge clk) begin
	sign<=~(xsr[WX*WX-1]^ysr[WY*WX-1]);
end
wire issigned=SIGNED;
assign z=SIGNED ? {{2{sign}},sumsr[(WABSX+WABSY)*(WABSX+1)-1:(WABSX+WABSY)*WABSX]} : sumsr[(WX+1)*(WX+WY)-1:WX*(WX+WY)];

endmodule
