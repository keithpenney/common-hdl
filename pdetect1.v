`timescale 1ns / 1ns
module pdetect(clk, ang_in, strobe_in, ang_out, strobe_out, reset);
parameter WIDTH=17;
input clk;
input [WIDTH-1:0] ang_in;
input strobe_in;
output [WIDTH-1:0] ang_out;
output strobe_out;
input reset;
reg signed [WIDTH-1:0] ang_out_r=0;
reg signed strobe_out_r=0;
reg [1:0] state=0;
// use localparameter and not depend on the state coding.
// try not to be too smart to make the code more readable
localparam S_LINEAR=2'b00;
localparam S_CLIP_P=2'b10;
localparam S_CLIP_N=2'b11;
localparam QUAD0=2'b00;
localparam QUAD1=2'b01;
localparam QUAD2=2'b10;
localparam QUAD3=2'b11;

reg [1:0] prev_quad=0;
wire [1:0] quad = ang_in[WIDTH-1:WIDTH-2];
wire trans_pn = (prev_quad==QUAD1) & (quad==QUAD2);
wire trans_np = (prev_quad==QUAD2) & (quad==QUAD1);

reg [1:0] next=0;
always @(*) begin
	if (reset) next=S_LINEAR;
	else if (trans_pn & (state==S_LINEAR)) next=S_CLIP_P;
	else if (trans_np & (state==S_LINEAR)) next=S_CLIP_N;
	else if (trans_pn & (state==S_CLIP_N)) next=S_LINEAR;
	else if (trans_np & (state==S_CLIP_P)) next=S_LINEAR;
	else next=state;
end
wire nextclip = next==S_CLIP_P|next==S_CLIP_N;
wire [WIDTH-1:0] clipv = next==S_CLIP_P ? {1'b0,{WIDTH-1{1'b1}}} :
						next==S_CLIP_N ? {1'b1,{WIDTH-1{1'b0}}} : 0;
always @(posedge clk) begin
	if (strobe_in) begin
		prev_quad <= quad;
		state <= next;
		ang_out_r <= nextclip ? clipv : ang_in;
	end
	strobe_out_r<= strobe_in;
end
assign ang_out=ang_out_r;
assign strobe_out=strobe_out_r;
endmodule
