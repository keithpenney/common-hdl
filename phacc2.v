`timescale 1ns / 1ns
module phacc2(input clk
,input reset
,input signed [PHASE1WIDTH-1:0] phsteph_offset
,output [PHASE1WIDTH-1:0] phase_h
,output [PHASE1WIDTH-1:0] phase_offset
,input [PHASE1WIDTH-1:0] phinit
,output sample1
);
reg [PHASE1WIDTH-1:0] phase_offset_r=0;
//parameter WIDTH=20;
//localparam ZWIDTH=WIDTH+1;
//localparam PHASE1WIDTH=ZWIDTH+1;
parameter PHASE1WIDTH=31;
parameter PHSTEPH=16268815;
parameter PHSTEPL=2108;
parameter MODULO=4;
wire [PHASE1WIDTH-1:0] phsteph= PHSTEPH;
wire [12-1:0] phstepl= PHSTEPL;
reg carry=0;
reg reset1=0;
reg [PHASE1WIDTH-1:0] phase_hr=0;
reg [PHASE1WIDTH-1:0] phase_hr_d=0;
reg [11:0] phase_l=0;
reg carryh=0;
reg signed [PHASE1WIDTH-1:0] phsteph_offset_d=0;
assign sample1=phase_hr_d[PHASE1WIDTH-1]&~phase_hr[PHASE1WIDTH-1];
reg [31:0] sampcnt=0;
always @(posedge clk) begin
	reset1 <= reset;
	phase_hr_d<=reset ? 0 : phase_hr;
	phsteph_offset_d<=phsteph_offset;
	{carry, phase_l} <= reset ? 13'b0 : ((carry ? MODULO : 12'b0) + phase_l + phstepl);
	{carryh,phase_hr} <= reset1 ? {phinit,1'b0} : (phase_hr + phsteph + carry);
	phase_offset_r <= reset1 ? 0 : phase_offset_r + phsteph_offset_d;
	sampcnt<=sample1 ? 0 : sampcnt+1;
end
assign phase_h = phase_hr;
assign phase_offset=phase_offset_r;
endmodule
