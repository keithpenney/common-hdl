module piloop5 #(parameter DEBUG="true"
,parameter KPKISHIFTMAX=16
,parameter KISHIFTSTATIC=16
,parameter GWIDTH=16
,parameter DWIDTH=16
,parameter INTEWIDTH=16
,localparam SHIFTWIDTH=clog2(KPKISHIFTMAX)+1
)(input clk
,input signed [DWIDTH-1:0] vin
,input stb_vin
,input signed [DWIDTH-1:0] vopen
,input signed [DWIDTH-1:0] vclose
,input signed [GWIDTH-1:0] kp
,input [SHIFTWIDTH-1:0] kpshift
,input signed [GWIDTH-1:0] ki
,input [SHIFTWIDTH-1:0] kishift
,output signed [DWIDTH-1:0] vout
,output stb_vout
,input [0:0] reset
,input [0:0] closeloop
,output [DWIDTH-1:0]  dbverr
);
function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction
localparam MSBGUARD=1;
localparam LSBGUARD=1;
reg signed [DWIDTH+MSBGUARD-1:0] verrp1=0;
wire signed [DWIDTH-1:0] verr_w;
reg signed [DWIDTH-1:0] verr=0;
assign dbverr=verr;
sat #(.WIN(DWIDTH+MSBGUARD),.WOUT(DWIDTH)) satverr(.din(verrp1),.dout(verr_w));
wire signed [GWIDTH+DWIDTH-1:0] multp;
wire signed [GWIDTH+DWIDTH-1:0] multi;
reg signed [KPKISHIFTMAX+GWIDTH+DWIDTH+1-1:0] multisft=0;
reg signed [GWIDTH+DWIDTH-1:0] multpsft=0;

reg  signed [INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH+1-1:0] vinter=0;
wire signed [INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH  -1:0] vintel;
sat #(.WIN(INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH+1),.WOUT(INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH))
satvinter(.din(vinter),.dout(vintel));
wire signed [INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH+1-1:0] vintese=$signed({vintel[INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH-1],vintel});

wire signed [INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH+1-1:0] multise=$signed({{(INTEWIDTH+1){multisft[KPKISHIFTMAX+GWIDTH+DWIDTH-1]}},multisft});
wire signed [INTEWIDTH+KPKISHIFTMAX+GWIDTH+DWIDTH+1-1:0] vopense=vopen<<<(KISHIFTSTATIC+KPKISHIFTMAX);
reg signed [DWIDTH+MSBGUARD:0] voutsum_r=0;
wire signed [DWIDTH+MSBGUARD-1:0] vouti;
wire signed [DWIDTH+MSBGUARD-1:0] voutp;
wire [GWIDTH+DWIDTH-1:0] voutis=(vintel>>>(KISHIFTSTATIC+KPKISHIFTMAX));
wire [GWIDTH+DWIDTH-1:0] voutps=multpsft;
sat #(.WIN(GWIDTH+DWIDTH),.WOUT(DWIDTH+MSBGUARD))
satvoutp(.din(voutps),.dout(voutp));
sat #(.WIN(GWIDTH+DWIDTH),.WOUT(DWIDTH+MSBGUARD))
satvouti(.din(voutis),.dout(vouti));
reg signed [GWIDTH-1:0] kir=0;
reg signed [GWIDTH-1:0] kpr=0;
assign	multp= reset|~closeloop ? 0 : ($signed(verr) * $signed(kpr));
assign	multi= reset|~closeloop ? 0 : ($signed(verr) * $signed(kir));
wire signed [DWIDTH-1:0] vout_w;
reg signed [DWIDTH-1:0] vout_r=0;

always @(posedge clk) begin
	verr<=verr_w;
	kpr<=kp;
	kir<=ki;
	verrp1<=reset | ~closeloop ? 0 : stb_vin ?  vclose-vin : 0;
	multisft <= reset|~closeloop ? 0 : (multi <<< (KPKISHIFTMAX-kishift));
	multpsft <= reset|~closeloop ? 0 : (multp >>> (kpshift));
	vinter <= reset ? 0 : (closeloop ? (vintese+multise) : vopense);
	voutsum_r <= reset ? 0 :(vouti+voutp);
	vout_r<=vout_w;
end
sat #(.WIN(DWIDTH+MSBGUARD+1),.WOUT(DWIDTH))
satvout (.din(voutsum_r),.dout(vout_w));
assign vout=vout_r;
reg_delay1 #(.DW(1), .LEN(5)) reg_delay(.clk(clk), .gate(1'b1), .din(stb_vin),   .dout(stb_vout),.reset(reset));
endmodule
