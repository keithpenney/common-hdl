`include "constants.vams"
`timescale 1ns / 10ps
module piloop5_tb();
function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction

reg dspclk;
integer cc;
initial begin
	$dumpfile("piloop5.vcd");
	$dumpvars(7,piloop5_tb);
	for (cc=0; cc<60000; cc=cc+1) begin
		dspclk=0; #5;
		dspclk=1; #5;
	end
	$finish();
end
parameter KPKISHIFTMAX=16;
parameter KISHIFTSTATIC=16;
parameter GWIDTH=16;
parameter DWIDTH=16;
parameter INTEWIDTH=16;
localparam SHIFTWIDTH=clog2(KPKISHIFTMAX)+1;
// piloop5 cross wire
wire clk=dspclk;
reg [31:0] clkcnt=0;
always @(posedge clk) begin
	clkcnt<=clkcnt+1;
end
wire [0:0] closeloop=1;
wire [GWIDTH-1:0] ki=16'h110;
wire [SHIFTWIDTH-1:0] kishift=0;
wire [GWIDTH-1:0] kp=-1;
wire [SHIFTWIDTH-1:0] kpshift=4;
wire [0:0] reset=cc<20;
wire [DWIDTH-1:0] vclose=16'h1000;
wire [DWIDTH-1:0] vout;
wire [DWIDTH-1:0] vin=vout;//clkcnt[3:0]==0 ? 100 : 0 ;//vout;
wire [DWIDTH-1:0] vopen=100;
wire stb_vout;
piloop5 #(.KPKISHIFTMAX(KPKISHIFTMAX),.KISHIFTSTATIC(KISHIFTSTATIC),.GWIDTH(GWIDTH),.DWIDTH(DWIDTH),.INTEWIDTH(INTEWIDTH))
piloop5(.clk(clk),.closeloop(closeloop),.ki(ki),.kishift(kishift),.kp(kp),.kpshift(kpshift),.reset(reset),.vclose(vclose),.vin(vin),.vopen(vopen),.vout(vout),.stb_vin(clkcnt[3:0]==0),.stb_vout(stb_vout));
reg [DWIDTH-1:0] vout_r=0;
always @(posedge clk) begin
	if (stb_vout) begin
		vout_r<=vout;
	end
end
endmodule
