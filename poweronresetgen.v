module poweronresetgen #(parameter POWERONRESET=10)
(input clk
,output reset
,output reseted
);
reg reset_r=0;
reg reset_d=0;
//wire reset;
reg [31:0] resetcnt=0;
reg reseted_r=0;
always @(posedge clk) begin
    resetcnt<=resetcnt+(reset_r ? 1'b0 : 1'b1);
	reset_r<=resetcnt>=POWERONRESET;
    reset_d<=reset_r;
    if (reset)
        reseted_r=1'b1;
end
assign reset = reset_r& ~reset_d;
assign reseted=reseted_r;
endmodule

