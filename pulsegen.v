`timescale 1ns / 1ns
module pulsegen(input clk
,input reset
,input sample1
,input [0:0] ext_trig_sel
,input [0:0] ext_trig
,input [27:0] rep_period
,input [23:0] pulse_length
,output pulse
,output pulse_rising
,output pulse_faling
,output [0:0] pulse_sync
);

reg [27:0] repcnt=10;
reg [23:0] pulsecnt=10;
wire pulsecnt0=~|pulsecnt;
wire repcnt0=~|repcnt;
reg cw=0;
reg cwtrig=0;
reg syncpulse_d=0;
wire syncpulse = ext_trig_sel ? ext_trig : cw ? cwtrig : |pulsecnt;
reg pulse_r=0;
assign pulse = pulse_r;
always @(posedge clk) begin
	pulse_r <= ext_trig_sel ? ext_trig : cw ? 1'b1 : |pulsecnt ;
	cw <= (rep_period<=pulse_length);
	syncpulse_d<=syncpulse;
	if (sample1) begin
		pulsecnt<= reset |(pulsecnt0&repcnt0) ? pulse_length : (pulsecnt0 ? 0 : pulsecnt-1);
		repcnt <= reset | (repcnt0) ? rep_period : repcnt-1;
		cwtrig <= (repcnt0 & pulsecnt0) ? ~cwtrig : cwtrig;
	end
end

assign pulse_rising= syncpulse & ~syncpulse_d;
assign pulse_faling=~syncpulse &  syncpulse_d;
assign pulse_sync=syncpulse;
endmodule
