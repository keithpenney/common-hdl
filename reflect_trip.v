`timescale 1ns / 1ns

module reflect_trip(
	input clk,
	input signed [19:0] d_in,
	input [15:0] thresh_init,
	input [15:0] thresh_noise,
	input [15:0] decay_coef,
	input rf_on,
	input tick,
	output trip,
	output trip_display
);

reg signed [35:0] m_full=0;
wire signed [18:0] m0 = m_full[34:16];
reg signed [18:0] m1=0;
reg signed [19:0] mag=0;
reg tick1=0, tick2=0;
// Input 20-bit signed full-scale value is 2^9*22^2 = 247808
// (verified with loop_pipe simulation).
// But could get nearly twice that with heavy clipping on ADC.
// It's easier to keep guard bits than to add a clipping step here.
wire signed [17:0] d_in0 = d_in[19:2];
always @(posedge clk) begin
	m_full <= d_in0 * d_in0;
	m1 <= m0;
	tick1 <= tick;
	tick2 <= tick1;
	// (247808/4)^2/2^16*2 = 117128 fits in 18-bit signed without overflow.
	// End up with 20 bits to handle saturation effects discussed above.
	// But useful full-scale is only (247808/4)^2/2^16 = 58564, so use
	// 16-bit unsigned for the thresholds below.
	if (tick2) mag <= m0 + m1;
end

reg [16:0] thresh=0;
// Keep 16 lsb guard bits on thresh_exp, so we don't throw away bits from
// the product and accumulate round-off error.  thresh_exp will continue
// to decay away, maybe not so accurately, until the upper 16 bits of
// thresh_exp (as contributed to the thresh value) reaches zero.
reg [31:0] thresh_exp=0;
reg trip_r=0, trip_display_r=0, rf_on1=0, rf_on2=0, rf_on3=0;
reg [31:0] thresh_prod=0;
reg exceeded=0;
always @(posedge clk) begin
	rf_on1 <= rf_on;
	rf_on2 <= rf_on1;
	rf_on3 <= rf_on2;
	if (tick) thresh_exp <= thresh_exp - thresh_prod;
	//if (tick) $display("thresh_exp %d",thresh_exp);
	if (rf_on & ~rf_on1) thresh_exp <= {thresh_init,16'b0};
	thresh <= thresh_exp[31:16] + thresh_noise;
	thresh_prod <= thresh_exp[31:16] * decay_coef;
	exceeded <= mag > thresh;
	if (exceeded & rf_on1 & rf_on2 & rf_on3) trip_r <= 1;
	if (~rf_on & rf_on1) begin  // trailing edge of RF pulse
		trip_display_r <= trip_r;
		trip_r <= 0;
	end
end
assign trip = trip_r;
assign trip_display = trip_display_r;

endmodule
