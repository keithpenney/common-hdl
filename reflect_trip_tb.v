`timescale 1ns / 1ns

module reflect_trip_tb;

reg clk;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("reflect_trip.vcd");
		$dumpvars(5,reflect_trip_tb);
	end
	for (cc=0; cc<1900; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
	$finish();
end

// The following setup gives enough ticks per pulse to see the relevant
// activity, but does not line up with the physical system.
reg rf_go=0, rf_pulse=0, tick=0;
reg signed [19:0] d_in, a1;
always @(posedge clk) begin
	rf_go <= (cc%800) == 50;
	rf_pulse <= (cc%400) > 100;
	tick <= (cc%10)==6;
	a1 = cc>450 ? 124000 : 62000;
	d_in <= ((cc%10)==6 || (cc%10)==7) ? a1 : 16'bx;
end

wire reflect_fault, rf_master, rf_on;
wire rf_permit=1;

// All lines in this stanza are directly cut-and-pasted from atiming.v
reg rf_master_r=0;
reg rf_on_r=0, rf_on_prev=0;
wire turn_off = ~rf_permit | reflect_fault;
always @(posedge clk) begin
	if (rf_go) rf_master_r <= 1;
	if (turn_off) rf_master_r <= 0;
	rf_on_r <= rf_pulse & rf_master;
end
assign rf_master = rf_master_r;
assign rf_on = rf_on_r;

// Suppose the cavity has a time constant = 24.0 us, so amplitude falls away
// as a0*exp(-t/(24.0 us)), but power falls as a0^2*exp(-t/(12.0 us)).
// The nominal sample tick time for this subsystem matches the decay waveform
// module, 9.79 ns * 22 * (decay_samp_per=2) = 0.43 us.
// Thus a realistic value for decay_coef is (0.43 us)/(12.0 us)*2^16 = 2350
wire [15:0] decay_coef = 16'd2350;

wire signed [15:0] d_out;
wire trip;
reflect_trip mut(
	.clk(clk), .d_in(d_in), .rf_on(rf_on), .tick(tick),
	.thresh_init(16'd50000), .decay_coef(decay_coef), .thresh_noise(16'd500),
	.trip(reflect_fault)
);

// I know the module scales its arithmetic right, because the value of
// thresh_exp seen in simulation after 50000 is 48208, which is approximately
// 50000*(1-0.43/12.0) = 48208.3

endmodule
