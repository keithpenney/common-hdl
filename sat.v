module sat #(// WIN should > WOUT
parameter WIN=17
,parameter WOUT=16
)
(input signed [WIN-1:0] din
,output signed [WOUT-1:0] dout
);
assign dout = $signed((~|din[WIN-1:WOUT-1] | &din[WIN-1:WOUT-1]) ? din[WOUT-1:0] : {din[WIN-1],{(WOUT-1){~din[WIN-1]}}});
endmodule
