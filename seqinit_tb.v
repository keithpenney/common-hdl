//`include "constants.vams"
//`include "xc7vx485tffg1761pkg.vh"
`timescale 1ns / 100ps
module seqinit_tb(
//seqinit ifseqinit
);

reg sysclk=0;
integer cc;
initial begin
	$dumpfile("seqinit.vcd");
	$dumpvars(17,seqinit_tb);
//    forever #(2.5) sysclk=~sysclk;
	for (cc=0; cc<3800; cc=cc+1) begin
		sysclk=0; #2.5;
		sysclk=1; #2.5;
	end
	$finish();
end
reg [31:0] sysclkcnt=0;
always @(posedge sysclk) begin
	sysclkcnt<=sysclkcnt+1;
end
reg clk125=0;
initial begin
    forever #(4) clk125=~clk125;
end

wire [36:0] i2ccmd;
wire [32:0] resultwire=33'h1deadbeef;
wire i2cstart;
wire running;
reg [31:0] runcnt=0;
always @(posedge sysclk) begin
	runcnt<=i2cstart ? 50 : runcnt-running;
end
assign running=|runcnt;
seqinit #(.NSTEP(3),.INITWIDTH(32+4+1),.INITLENGTH({16'd3,16'd11,16'd21}),.RESULTWIDTH(33)
,.INITCMDS(  {{1'h1,4'h2,32'he8080000}  // read eeprom for mac and ip address
,{1'h0,4'h2,32'ha8000000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8010000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8020000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8030000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8040000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8050000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8060000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8070000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8080000}
,{1'h1,4'h2,32'ha9000000}
,{1'h0,4'h2,32'ha8090000}
,{1'h1,4'h2,32'ha9000000}
})
)
i2cinit(.clk(sysclk)
,.areset({sysclkcnt==150,sysclkcnt==500,sysclkcnt==2000})
,.cmd(i2ccmd)
,.resultwire(resultwire)
,.start(i2cstart)
,.runing(running)
,.initdone()
,.result(i2cresult)
,.livecmd(0)
,.livestart(0)
);



endmodule
