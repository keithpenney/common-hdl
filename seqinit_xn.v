module seqinit #(parameter INITWIDTH=27
,parameter INITLENGTH={16'd4}
,parameter RESULTWIDTH=1
,parameter INITCMDS=0
,parameter NSTEP=1
)

(input clk
,input [NSTEP-1:0] areset
,input runing
,output [INITWIDTH-1:0] cmd
,input [RESULTWIDTH-1:0] resultwire
,output [RESULTWIDTH-1:0] result
,output start
,output [NSTEP-1:0] initdone
,input [INITWIDTH-1:0] livecmd
,input livestart
,output [NSTEP-1:0] dbsreset
,output [3:0] dbstate
,output [3:0] dbnext
);

function integer clog2;
    input integer value;
    begin
        value = value-1;
        for (clog2=0; value>0; clog2=clog2+1)
            value = value>>1;
    end
endfunction

reg [INITWIDTH-1:0] cmd_r=0;
reg [RESULTWIDTH-1:0] result_r=0;

wire reset;
wire [NSTEP-1:0] sreset;
areset #(.WIDTH(NSTEP)) asynreset(.clk(clk),.areset(areset),.sreset(sreset));
//assign reset=areset|sreset;
assign dbsreset=reset;
localparam TOTALLENGTH=INITLENGTH[15:0];
localparam INITLENWIDTH= clog2(TOTALLENGTH);
reg [INITLENWIDTH-1:0] initcnt=0;
localparam PREP=4'd0;
localparam START=4'd1;
localparam RUN=4'd2;
localparam NEXT=4'd3;
localparam NEXTRESET=4'd4;
localparam WAITRESET=4'd5;
localparam DONE=4'd6;
reg [NSTEP-1:0] done=0;
reg [3:0] state=DONE;
reg [3:0] next=DONE;
assign dbstate=state;
assign dbnext=next;
wire [15:0] tolength;
reg prep=0;
always @(posedge clk) begin
	if (sreset)
		state<=PREP;
	else begin
		state<=next;
	end
end
always @(*) begin
	case (state)
		PREP: next = prep ? START : PREP;
		START: next = runing ? RUN : START;
		RUN: next = runing ? RUN : NEXT;
		NEXT: next= initcnt==tolength ? ((|nstep) ? NEXTRESET : DONE ): PREP ;
		NEXTRESET: next= WAITRESET;
		WAITRESET: next = reset ? PREP : WAITRESET;
		DONE: next =DONE;
	endcase
end
reg [INITWIDTH-1:0] initcmd=0;
reg start_r=0;

reg [15:0] nstep=NSTEP-1;
wire r1=(sreset >> nstep) & 1'b1;
assign reset=(sreset >> nstep) & 1'b1;
assign tolength=INITLENGTH >> (nstep<<4);//[(nstep+1)*16-1:nstep*16];
assign {opr1w0_i,phyaddr_i,regaddr_i,datatx_i}=initcmd;
always @(posedge clk) begin
	//reset<=rst;
	if (sreset[NSTEP-1]) begin
		initcnt<=0;
		nstep<=NSTEP-1;
		initcmd<=0;
		done<=0;
	end
	else begin
		case (next)
			PREP: begin
				initcmd<=INITCMDS[((TOTALLENGTH-initcnt)-1)*INITWIDTH+:INITWIDTH];
				prep<=1'b1;
			end
			START: begin
				prep<=1'b0;
				start_r<=1'b1;
			end
			RUN: begin
				start_r<=1'b0;
			end
			NEXT: begin
				initcnt<=initcnt+1;
			end
			NEXTRESET: begin
				nstep<=nstep-1;
				done[nstep]<=1'b1;
			end
			DONE: begin
				initcmd<=livecmd;
				start_r<=livestart;
				done[nstep]<=1'b1;
			end
		endcase
	end
	result_r<=resultwire;
end
assign start=start_r;
assign cmd=initcmd;
assign initdone=done;
assign result=result_r;
endmodule
