`timescale 1ns / 1ns
// Parallel to serial
// Snapshot data_in at samp and serialize it to stream of DWI width stream.
// gatestart is one cycle before the gate
// gatestop is the last cycle of the gate
// gate is the valid of stream
// gate_sr is the internal shift register for the gate calculation, can also be used outside to get part of the stream.
module serialize1(clk,samp,data,stream,gate,gatestart,gate_sr,gatestop,reset);
parameter DWI=28;  // result width
parameter NCHAN=12;
parameter DELAY=0;
parameter MSBFIRST=0;
	input clk;  // timespec 8.4 ns
	input samp;
	input signed [NCHAN*DWI-1:0] data;
	output signed [DWI-1:0] stream;
	output gate;
	output gatestart;
	output gatestop;
	output [NCHAN-1:0] gate_sr;
	input reset;

reg [NCHAN-1:0] gate_reg;
wire sampout;
reg sampout_d=0;
reg_delay1 #(.dw(1),.len(DELAY)) sampdelay(.clk(clk),.gate(1'b1),.din(samp),.dout(sampout),.reset(reset));
reg signed [NCHAN*DWI-1:0] stream_reg=0;
always @(posedge clk) begin
	sampout_d<=sampout;
	gate_reg <= sampout ? {{(NCHAN-1){1'b0}},1'b1} : {gate_reg[NCHAN-2:0],1'b0};
end
generate
if (MSBFIRST==1) begin
	always @(posedge clk) begin
		stream_reg <= reset ? 0 : samp ? data : (sampout|gate)? {stream_reg[NCHAN*DWI-DWI-1:0],{DWI{1'b0}}} : stream_reg;
	end
	assign stream = stream_reg[NCHAN*DWI-1:(NCHAN*DWI-DWI)];
end
else begin
	always @(posedge clk) begin
		stream_reg <= reset ? 0 : samp ? data : (sampout|gate)? {{DWI{1'b0}},stream_reg[NCHAN*DWI-1:DWI]} : stream_reg;
	end
	assign stream = stream_reg[DWI-1:0];
end
endgenerate
assign gate = |gate_reg;
assign gatestart = sampout_d;
assign gatestop = gate_reg[NCHAN-1];
assign gate_sr = gate_reg;
endmodule
