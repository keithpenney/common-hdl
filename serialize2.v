`timescale 1ns / 1ns
// merge two streams
// if they are overlaped to each other, output error

module serialize2(input clk
,input [0:0] gdin
,input signed [dwi-1:0] din
,input [gwi-1:0] gsin
,input signed [dwi-1:0] streamin
,output [gwi-1:0] gsout
,output signed [dwi-1:0] streamout
,output error
);
parameter dwi=28;  // result width
parameter gwi=1;

assign streamout = gdin? din : streamin;
assign gsout[0] = gdin? gdin : gsin[0];
generate
	if (gwi>1) begin:gsouthighbit
		assign gsout[gwi-1:1] = gsin[gwi-1:1];
	end
endgenerate
assign error = dwi & din;

//assign outstream = datagate ? data : instream;
//assign outgate = datagate ? datagate : ingate;
//assign outgatestop = datagatestop;
//assign outgatestart = ingatestart;
//assign error = datagate & ingate;

endmodule
