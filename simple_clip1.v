`timescale 1ns / 1ns

module simple_clip1(
	input clk,
	input signed [WIDTH-1:0] d_in,
	input signed [WIDTH-1:0] clipv,
	output signed [WIDTH-1:0] d_out,
	output clipped  // goes to a red light?  Probably with some latching.
	,input reset
);
parameter WIDTH=16;
// First pipeline step
reg cmp_hi=0, cmp_lo=0;
reg signed [WIDTH-1:0] d_hold=0;
wire positive=(clipv>(-clipv));
wire signed [WIDTH-1:0] clipv_hi=positive ? clipv : -clipv;
wire signed [WIDTH-1:0] clipv_lo=positive ? -clipv : clipv;
always @(posedge clk) begin
	cmp_hi <= d_in > clipv_hi;
	cmp_lo <= d_in < clipv_lo;
	d_hold <= d_in;
end

// Second pipeline step
reg flag=0;
reg signed [WIDTH-1:0] result=0;
wire clipnow=cmp_hi | cmp_lo ;
always @(posedge clk) begin
	result <= cmp_hi ? clipv_hi : cmp_lo ? (clipv_lo) : d_hold;
	flag <= reset ? clipnow : (flag | clipnow);
end
assign d_out = result;
assign clipped = flag;

endmodule
