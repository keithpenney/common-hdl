`timescale 1ns / 1ns
`include "constants.vams"

module simple_clip_tb;

reg clk;
integer cc;
reg dump;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("simple_clip.vcd");
		$dumpvars(5,simple_clip_tb);
	end
	dump = $test$plusargs("dump");
	for (cc=0; cc<500; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
	$finish();
end

reg signed [16:0] d_in=0;
reg [14:0] clipv=6000;

real osc;
integer ramp;
reg [3:0] theta=0;
always @(posedge clk) begin
	theta <= theta==10 ? 0 : theta+1;
	osc = $cos(theta*2.0*`M_PI*2.0/11.0);
	ramp = 130*cc*osc;
	d_in <= ramp;
end

wire signed [15:0] d_out;
wire clipped;
simple_clip dut(.clk(clk), .d_in(d_in), .clipv(clipv),
	.d_out(d_out), .clipped(clipped)
);

wire signed [15:0] d_post;
clipfilt post(.clk(clk), .d_in(d_out), .d_out(d_post));

always @(negedge clk) if (dump) $display("%d %d %d",d_in,d_out,d_post);

endmodule
