`timescale 1 ns/10 ps
module source6 (
	clk,cos,sin,ampi,ampq,d_outre,d_outim
	//ai, bi, ar, br, pi, pr
);
input clk;
input signed [WIDTH-1:0] cos;
input signed [WIDTH-1:0] sin;
input signed [WIDTH-1:0] ampi;
input signed [WIDTH-1:0] ampq;
output signed [WIDTH-1:0] d_outre;
output signed [WIDTH-1:0] d_outim;
wire signed [17:0] ai= {{18-WIDTH{sin[WIDTH-1]}},sin};
wire signed [17:0] bi= {{18-WIDTH{ampq[WIDTH-1]}},ampq};
wire signed [17:0] ar= {{18-WIDTH{cos[WIDTH-1]}},cos};
wire signed [17:0] br= {{18-WIDTH{ampi[WIDTH-1]}},ampi};
reg signed [47:0] pi=0;
reg signed [47:0] pr=0;
reg signed [17:0] ar1=0;
reg signed [17:0] ar2=0;
reg signed [17:0] ai1=0;
reg signed [17:0] ai2=0;
reg signed [17:0] br1=0;
reg signed [17:0] br2=0;
reg signed [17:0] bi1=0;
reg signed [17:0] bi2=0;
parameter FPGA="NONE";
always @(posedge clk) begin
	ar1<=ar;
	ar2<=ar1;
	ai1<=ai;
	ai2<=ai1;
	br1<=br;
	br2<=br1;
	bi1<=bi;
	bi2<=bi1;
end
//dsp48 #(.PREADD(0))dspa(.clk(clk),.reset(1'b0),.b(arw),.d(aiw),.a(biw),.c(48'b0),.p(pa));
//dsp48 #(.PREADD(0))dspb(.clk(clk),.reset(1'b0),.b(brw),.d(biw),.a(arw),.c(pa),.p(pb));
//dsp48 dspc(.clk(clk),.reset(1'b0),.b(brw),.d( biw),.a(aiw),.c(pa),.p(pc));
reg rst=0;
parameter WIDTH=16;
wire [47:0] pa,pb,pc;
/*DSP48A1 #(.A0REG ( 1 ),.A1REG ( 1 ),.B0REG ( 1 ),.B1REG ( 1 ),.CARRYINREG ( 0 ),.CARRYINSEL ( "OPMODE5" ),.CREG ( 1 ),.DREG ( 1 ),.MREG ( 1 ),.OPMODEREG ( 0 ),.PREG ( 1 ),.RSTTYPE ( "SYNC" ),.CARRYOUTREG ( 0 ))
dspa( .A(bi),.B(ai),.D(ar),.C(48'b0),.CLK(clk),.PCIN(48'b0)
,.CARRYIN(1'b0),.OPMODE({1'b0,1'b1,1'b0,1'b1,2'h0,2'h1})
,.RSTA(rst),.RSTB(rst),.RSTM(rst),.RSTP(rst),.RSTC(rst),.RSTD(rst),.RSTCARRYIN(rst),.RSTOPMODE(rst)
,.CEA(1'b1),.CEB(1'b1),.CEM(1'b1),.CEP(1'b1),.CEC(1'b1),.CED(1'b1),.CECARRYIN(1'b0),.CEOPMODE(1'b0)
,.BCOUT(),.PCOUT(),.P(pa),.CARRYOUT(),.CARRYOUTF());

DSP48A1 #(.A0REG ( 1 ),.A1REG ( 1 ),.B0REG ( 1 ),.B1REG ( 1 ),.CARRYINREG ( 0 ),.CARRYINSEL ( "OPMODE5" ),.CREG ( 1 ),.DREG ( 1 ),.MREG ( 1 ),.OPMODEREG ( 0 ),.PREG ( 1 ),.RSTTYPE ( "SYNC" ),.CARRYOUTREG ( 0 ))
dspb( .A(ar2),.B(bi2),.D(br2),.C(pa),.CLK(clk),.PCIN(48'b0)
,.CARRYIN(1'b0),.OPMODE({1'b0,1'b1,1'b0,1'b1,2'h3,2'h1})
,.RSTA(rst),.RSTB(rst),.RSTM(rst),.RSTP(rst),.RSTC(rst),.RSTD(rst),.RSTCARRYIN(rst),.RSTOPMODE(rst)
,.CEA(1'b1),.CEB(1'b1),.CEM(1'b1),.CEP(1'b1),.CEC(1'b1),.CED(1'b1),.CECARRYIN(1'b0),.CEOPMODE(1'b0)
,.BCOUT(),.PCOUT(),.P(pb),.CARRYOUT(),.CARRYOUTF());


DSP48A1 #(.A0REG ( 1 ),.A1REG ( 1 ),.B0REG ( 1 ),.B1REG ( 1 ),.CARRYINREG ( 0 ),.CARRYINSEL ( "OPMODE5" ),.CREG ( 1 ),.DREG ( 1 ),.MREG ( 1 ),.OPMODEREG ( 0 ),.PREG ( 1 ),.RSTTYPE ( "SYNC" ),.CARRYOUTREG ( 0 ))
dspc( .A(ai2),.B(bi2),.D(br2),.C(pa),.CLK(clk),.PCIN(48'b0)
,.CARRYIN(1'b0),.OPMODE({1'b0,1'b0,1'b0,1'b1,2'h3,2'h1})
,.RSTA(rst),.RSTB(rst),.RSTM(rst),.RSTP(rst),.RSTC(rst),.RSTD(rst),.RSTCARRYIN(rst),.RSTOPMODE(rst)
,.CEA(1'b1),.CEB(1'b1),.CEM(1'b1),.CEP(1'b1),.CEC(1'b1),.CED(1'b1),.CECARRYIN(1'b0),.CEOPMODE(1'b0)
,.BCOUT(),.PCOUT(),.P(pc),.CARRYOUT(),.CARRYOUTF());
*/
cmultiplier #(.FPGA(FPGA))
cmultiplier(.clk(clk),.rst(rst),.xi(ai),.xr(ar),.yi(bi),.yr(br),.zr(pb),.zi(pc));

always @(posedge clk ) begin
	pr<=pb;
    pi<=pc;
end
sat #(.WIN(48-WIDTH+1),.WOUT(WIDTH)) satre(.din(pr[47:WIDTH-1]),.dout(d_outre));
sat #(.WIN(48-WIDTH+1),.WOUT(WIDTH)) satim(.din(pi[47:WIDTH-1]),.dout(d_outim));
//assign d_outre=pr[2*WIDTH-2:WIDTH-1];
//assign d_outim=pi[2*WIDTH-2:WIDTH-1];
endmodule
