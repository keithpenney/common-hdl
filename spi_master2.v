module spi_master2 #(parameter ADDRWIDTH=16
,parameter DATAWIDTH=8
,parameter SCK_RISING_SHIFT=1
,parameter CSCATCH=1
)
(input clk
,output CS
,output CSB
,output SCK
,output SCKB
,output MOSI
,input	MISO
,output SDIOASSDO
,output SDIOASSDOB
,input sck
,input start
,input [ADDRWIDTH-1:0] addr
,input [DATAWIDTH-1:0] data
,input read
,output [ADDRWIDTH-1:0] sdo_addr
,output [DATAWIDTH-1:0] rdbk
,input readytostart
,output ready
);
localparam SCKCNT_WIDTH = clog2(ADDRWIDTH+DATAWIDTH+1);

function integer clog2;
    input integer value;
    begin
        value = value-1;
        for (clog2=0; value>0; clog2=clog2+1)
            value = value>>1;
    end
endfunction

reg [SCKCNT_WIDTH-1:0] sckcnt=0;
reg cs_r=0;
reg cs_r_d=0;
reg start_r=0;
reg start_d=0;
reg sck_d=0;
always @(posedge clk) begin
	start_d<=start;
	sck_d<=sck;
	cs_r_d<=cs_r;
	if (readytostart & start & ~start_d)
		start_r<=1;
	else if (CS)
		start_r<=0;
end

wire catchedge=SCK_RISING_SHIFT ? (sck&~sck_d) : (sck_d&~sck);
wire prepaedge=SCK_RISING_SHIFT ? (sck_d&~sck) : (~sck_d&sck);
reg [ADDRWIDTH+DATAWIDTH-1:0]  writereg=0;
reg [ADDRWIDTH+DATAWIDTH-1:0]  readreg=0;
reg [ADDRWIDTH+DATAWIDTH-1:0]  readresult=0;
reg write_r=0;
reg read_r=0;
wire csedge=CSCATCH ? catchedge : prepaedge;
always @(posedge clk) begin
	if (csedge) begin
		if (start_r) begin
			cs_r<=1'b1;
		end
	end
	if (catchedge) begin
		readreg<= CS ?  {readreg[ADDRWIDTH+DATAWIDTH-2:0], write_r ? MOSI : MISO} :0;
		if (start_r) begin
			sckcnt<=0;
		end
		else begin
			//sckcnt<=cs_r ? (sckcnt+1) : 0;
			sckcnt<=CS ? (sckcnt+1) : 0;
		end
		//if (start_r) begin
		//	write_r <= 1'b1;
		//end
	end
	else if (prepaedge) begin
		//if (start_r) begin
		//	write_r <= 1'b1;
		//end
		if (sckcnt==(ADDRWIDTH+DATAWIDTH)) begin
			cs_r<=1'b0;
			readresult<=readreg;
			read_r<=1'b0;
		end
		//if (sckcnt==ADDRWIDTH) begin
		if (sckcnt>=ADDRWIDTH) begin
			write_r <= CS ? ~read_r : 0;
		end
		else begin
			write_r <= 1'b1;
		end
		if (sckcnt==0) begin
			writereg<={addr,data};
			read_r<=read;
		end
		else begin
			writereg<= CS ? {writereg[ADDRWIDTH+DATAWIDTH-2:0],1'b0} : 0;
		end
	end
end
assign CS=cs_r&cs_r_d;
assign CSB=~CS;
assign MOSI=CS ? writereg[ADDRWIDTH+DATAWIDTH-1] : 1'b0;
assign SDIOASSDO=(CS&~write_r);
assign SDIOASSDOB=~SDIOASSDO;
assign SCK=cs_r&sck_d;
assign SCKB=~SCK;
assign {sdo_addr,rdbk}=readresult;
assign ready=~(start_r|CS);
endmodule
