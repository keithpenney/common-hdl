module spiinit
#(parameter AWIDTH=8
,parameter DWIDTH=8
,parameter LENGTH=3
,parameter ARRAYINIT=0
)
(input clk
,input stbin
,input ready
,input readytostart
,output stbout
,output addr
,input [ADWWIDTH-1:0] adw
,output [AWIDTH-1:0] aini
,output [DWIDTH-1:0] dini
,output [0:0] wini
,output [0:0] rini
,output [0:0] startini
,output [0:0] done
);
localparam ADWWIDTH=AWIDTH+DWIDTH+1;
parameter WLENGTH=$clog2(LENGTH)+1;
wire [ADWWIDTH*LENGTH-1:0] arrayin=ARRAYINIT;
reg [WLENGTH-1:0] index=0;
reg stbin_d=0;
reg stbout_r=0;
reg stbin_r=0;
reg stbin_r_d=0;
reg ready_d=0;
reg done_r=0;
reg ready_r=0;
reg ready_r_d=0;
reg started=0;
always @(posedge clk) begin
	stbin_d<=stbin;
	if (stbin&~stbin_d) begin
		stbin_r<=1;
		index<=0;
	end
	else if (readytostart)begin
		stbin_r<=0;
	end
	if (stbin&~stbin_d) begin
		started<=1'b1;
	end
	else if (stbout)
		started<=1'b0;
	stbin_r_d<=stbin_r;
	ready_d<=ready;
	if (ready&~ready_d) begin
		index<=index+1;
	end
	if (stbin&~stbin_d)
		done_r<=0;
	else if (stbout)
		done_r<=1;
	if (started&ready&~ready_d) begin
		ready_r<=1'b1;
	end
	else if (readytostart) begin
		ready_r<=1'b0;
	end
	ready_r_d<=ready_r;
end
assign {aini,dini,wini}=ARRAYINIT[(LENGTH-1-index)*ADWWIDTH+:ADWWIDTH];//adw[index];
assign rini=~wini;
assign startini=stbin_r_d|ready_r_d;//(~done_r&(ready&~ready_d));
assign stbout=(index==LENGTH-1)&ready&~ready_d;
assign done=done_r;
endmodule
//
//reg [WLENGTH-1:0] index=0;
//always @(posedge clk) begin
//	if (ready&stbin&~stbin_d) begin
//		index<=0;
//		stbout_r<=1'b0;
//	end
//	else if (index==LENGTH) begin
//		if (ready&~ready_d)begin
//			stbout_r<=1'b1;
//		end
//	end
//	else begin
//		if (ready&~ready_d) begin
//			index<=index+1;
//		end
//	end
//end
//assign stbout=stbout_r;
//[LENGTH*ADWWIDTH-1:0]
