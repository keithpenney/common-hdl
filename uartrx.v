module uartrx #(parameter DWIDTH=8
,parameter NSTOP=1)(input clk
,input [15:0] bauddiv2
,input rst
,input rxline
,output [DWIDTH-1:0] data
,output valid
,output busy
,output ready
,output error
,output rxbaudclk
,output [1:0] rxstate
,output [1:0] rxnext
,output [5:0] rxrxcnt
,output [15:0] rxbaudcnt
,output reg rxline_r=0
);

reg [31:0] bitcnt=0;
reg [31:0] bit0cnt=0;
reg [31:0] bit1cnt=0;
reg rxline_r2=0;
//reg rxline_r=0;
wire change=rxline_r2^rxline_r;
always @(posedge clk) begin
	rxline_r2<=rxline;
	rxline_r<=rxline;
	bitcnt<=change ? 0 : bitcnt+1;
	if (rxline_r) begin
		bit1cnt<=bit1cnt+1;
		bit0cnt<=0;
	end
	else begin
		bit0cnt<=bit0cnt+1;
		bit1cnt<=0;
	end
end

reg [15:0] baudcnt=0;
wire baudcnt0=~|baudcnt;
reg [1:0] baudclk=1'b1;
reg [1:0] baudclk_d=0;
wire stop;
wire start;
reg rxclk=0;
always@(posedge clk) begin
	if (rxclk) begin
		baudcnt<= baudcnt0 ? bauddiv2-1 : baudcnt-1;
		if (baudcnt0) begin
			baudclk<=baudclk+1'b1;
		end
	end
	else begin
		baudcnt<=bauddiv2-1;
		baudclk<=1'b1;
	end
	baudclk_d<=baudclk;
	if (start) begin
		rxclk<=1;
	end
	else begin
		if (stop) begin
			rxclk<=0;
		end
	end
end
wire baudclkrising=baudclk[0]&~baudclk_d[0];
wire baudclkfaling=~baudclk[0]&baudclk_d[0];
wire newbaudperiod=baudclk[1]^baudclk_d[1];
reg error_r=0;
localparam IDLE=2'b00;
localparam START=2'b01;
localparam DATA=2'b10;
localparam STOP=2'b11;
reg [1:0] state=IDLE;
reg [1:0] next=IDLE;
assign start=(state==IDLE & bit0cnt==bauddiv2);
//assign stop=(state==STOP & baudclkfaling);
assign stop=(state==STOP & next==IDLE);
always @ (posedge clk) begin
	if (rst) begin
		state <= IDLE;
	end
	else begin
//		if (baudclkfaling) 
			state <= next;
	end
end
reg [5:0] rxcnt=0;
always @(*) begin
	case (state)
		IDLE: begin
			next= start ? START : IDLE;
		end
		START: begin
			next = (rxcnt==1) ? DATA : START;
		end
		DATA: begin
			next = (rxcnt==DWIDTH+1)? STOP : DATA; 
		end
		STOP: begin
			next = (rxcnt==1+DWIDTH+NSTOP) ? IDLE : STOP; 
		end
	endcase
end
reg [DWIDTH-1:0] datasr=0;
reg [DWIDTH-1:0] data_r=0;
reg rxbit=0;
always @(posedge clk) begin
	if (rst) begin
		datasr<=0;
	end
	else begin
		case (next)
			IDLE:begin
				datasr<=0;
			end
			START:begin
				datasr<=0;
			end
			DATA: begin
				if (baudclkrising) begin
					datasr<={rxline,datasr[DWIDTH-1:1]};
				end
			end
			STOP: begin
				data_r<=datasr;
			end
		endcase
	end
	if (next==IDLE) begin
		rxcnt<=0;
	end
	else begin
		if (baudclkfaling) begin
			rxcnt<=rxcnt+1;
		end
	end
end
assign ready=(state==IDLE && next==IDLE);
assign busy=~ready;
assign error=error_r;
assign data=data_r;
assign valid=stop;
assign rxbaudclk=baudclk;
assign rxstate=state;
assign rxnext=next;
assign rxrxcnt=rxcnt;
assign rxbaudcnt=baudcnt;
endmodule
