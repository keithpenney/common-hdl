module vectorpi1 #(parameter DEBUG="true"
,parameter WIDTH=16
,parameter KPKISHIFTMAX=16
,parameter KISHIFTSTATIC=16
,parameter GWIDTH=16
,parameter INTEWIDTH=16
,parameter UPDN=0
,parameter LOWIDTH=20
,parameter FPGA="NONE"
)(input clk
,input signed [WIDTH-1:0] vin
,input signed [WIDTH-1:0] vclosesetx
,input signed [WIDTH-1:0] vclosesety
,input signed [WIDTH-1:0] kp
,input [SHIFTWIDTH-1:0] kpshift
,input signed [WIDTH-1:0] ki
,input [SHIFTWIDTH-1:0] kishift
,output signed [WIDTH-1:0] voutx
,output signed [WIDTH-1:0] vouty
,input reset
,input  [5:0] freezecycle
,input signed [WIDTH-1:0] vopensetx
,input signed [WIDTH-1:0] vopensety
,input close_loop
,input [LOWIDTH-1:0] cos
,input [LOWIDTH-1:0] sin
,input [WIDTH-1:0] scale
,output signed [WIDTH-1:0] xoutrot
,output signed [WIDTH-1:0] youtrot
,input signed [WIDTH-1:0] xrot
,input signed [WIDTH-1:0] yrot
);
function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction
localparam SHIFTWIDTH=clog2(KPKISHIFTMAX)+1;
// Universal definition; note: old and new are msb numbers, not bit widths.
`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})
localparam WIDTHP1=WIDTH+1;
localparam ZWIDTH=WIDTH+1;
localparam ZWIDTHP1=ZWIDTH+1;
localparam NSTAGE=WIDTHP1;
wire signed [WIDTH-1:0] kpx=kp;
wire signed [WIDTH-1:0] kix=ki;
wire signed [WIDTH-1:0] kpy=UPDN ? -kp : kp;
wire signed [WIDTH-1:0] kiy=UPDN ? -ki : ki;
wire [WIDTH-1:0] xout,yout;
ppiq #(.WIDTH(WIDTH),.LOWIDTH(LOWIDTH))
ppiq(.clk(clk),.cos(cos),.sin(sin),.scale(scale),.vin(vin),.xout(xout),.yout(yout));
source6 #(.WIDTH(WIDTH),.FPGA(FPGA)) source6 (.clk(clk),.cos(xout),.sin(yout),.ampi(xrot),.ampq(yrot),.d_outre(xoutrot),.d_outim(youtrot));
piloop5 #(.KPKISHIFTMAX(KPKISHIFTMAX),.KISHIFTSTATIC(KISHIFTSTATIC),.GWIDTH(GWIDTH),.WIDTH(WIDTH),.INTEWIDTH(INTEWIDTH))
piloop5x(.clk(clk),.close_loop(close_loop),.ki(kix),.kishift(kishift),.kp(kpx),.kpshift(kpshift),.reset(reset),.vclose(vclosesetx),.vin(xoutrot),.vopen(vopensetx),.vout(voutx));
piloop5 #(.KPKISHIFTMAX(KPKISHIFTMAX),.KISHIFTSTATIC(KISHIFTSTATIC),.GWIDTH(GWIDTH),.WIDTH(WIDTH),.INTEWIDTH(INTEWIDTH))
piloop5y(.clk(clk),.close_loop(close_loop),.ki(kiy),.kishift(kishift),.kp(kpy),.kpshift(kpshift),.reset(reset),.vclose(vclosesety),.vin(youtrot),.vopen(vopensety),.vout(vouty));
endmodule
