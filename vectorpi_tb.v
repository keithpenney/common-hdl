`include "constants.vams"
`timescale 1ns / 10ps
module vectorpi_tb;
function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction
reg clk;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("vectorpi.vcd");
		$dumpvars(7,vectorpi_tb);
	end
	//  $dumpfile("vectorpi.vcd");
	//  $dumpvars(6,vectorpi_tb);
	for (cc=0; cc<10000; cc=cc+1) begin
		clk=0; #5.2;
		clk=1; #5.2;
	end
	$finish();
end
reg [LOWIDTH+1:0] offset;
always @(posedge clk) begin
	offset <= (cc>400) ? 2 : 0;
end


real vtcos;
real vtsin;
real phlodn_r;
reg [15:0]  vin0=0;
reg [15:0]  vin1=0;
reg [15:0]  vin2=0;
reg [15:0]  vin3=0;
reg [15:0]  vin4=0;
reg [15:0]  vin5=0;
reg [15:0]  vin6=0;
reg [15:0]  vin7=0;
reg [19:0] locos=0;
reg [19:0] losin=0;
reg [31:0] clkcnt=0;
parameter LOWIDTH=20;
always@(posedge clk) begin
	clkcnt<=clkcnt+1;
	phlodn_r<=clkcnt*22.0/132*`M_TWO_PI;
	vtcos<=$cos(phlodn_r);
	vtsin<=$sin(phlodn_r);
	vin0<=100*vtcos;
	vin1<=-2000*vtcos;
	vin2<=4000*vtcos;
	vin3<=(cc<1500) ? 8000*$cos(phlodn_r) : 8000*$cos(phlodn_r+10.0/360*`M_TWO_PI);//8000*vtcos;
end



localparam ADCWIDTH=16;
localparam WIDTH=16;
localparam ZWIDTH=WIDTH+1;
localparam CIC_PERIOD=132;
parameter NLO=6;
parameter RATIOWIDTH=8;
//parameter [NLO*RATIOWIDTH-1:0] RATIO = {8'd29,8'd84,8'd104,8'd203,8'd84,8'd203};
parameter PHSTEPH=195225786;
parameter PHSTEPL=744;
parameter [NLO*RATIOWIDTH-1:0] RATIO = {8'd2,8'd2,8'd2,8'd2,8'd2,8'd2};
//parameter PHSTEPH=16268815;
//parameter PHSTEPL=2108;
//parameter [NLO*RATIOWIDTH-1:0] RATIO = {8'd29,8'd2,8'd2,8'd2,8'd2,8'd2};
parameter LOWIDTHP1=LOWIDTH+1;
parameter real LOAMP = (2**(LOWIDTHP1-1))/1.64676025812107-3;
wire [LOWIDTHP1-1:0] loamp=LOAMP;
wire [LOWIDTHP1-1:0] lozero=0;
wire [LOWIDTHP1-1:0] lo185upx=loamp;
wire [LOWIDTHP1-1:0] lo185upy=0;
wire [LOWIDTHP1-1:0] lo1300upx=loamp;
wire [LOWIDTHP1-1:0] lo1300upy=0;
parameter MODULO=4;
wire [19:0] ddsa_phstep_h=0;
wire [LOWIDTH-1:0] cos185dn,cos185up,cos1300dn,cos1300up,cos185updrv,cos1300updrv;
wire [LOWIDTH-1:0] sin185dn,sin185up,sin1300dn,sin1300up,sin185updrv,sin1300updrv;
wire [LOWIDTH+2-1:0] ph185dn,ph185up,ph1300dn,ph1300up,ph185updrv,ph1300updrv;
wire [LOWIDTH+2-1:0] ph185dn0,ph185up0,ph1300dn0,ph1300up0,ph185updrv0,ph1300updrv0;
wire  reset=cc==1;
logen #(.NLO(NLO),.RATIOWIDTH(RATIOWIDTH),.RATIO(RATIO),.PHSTEPH(PHSTEPH),.PHSTEPL(PHSTEPL),.MODULO(MODULO),.WIDTH(LOWIDTH))
logen(.clk(clk),.phsteph_offset({ddsa_phstep_h,2'b0}),.reset(reset)
,.sin({sin185dn,sin185up,sin1300dn,sin1300up,cos185updrv,cos1300updrv})
,.cos({cos185dn,cos185up,cos1300dn,cos1300up,sin185updrv,sin1300updrv})
,.xlo({loamp,loamp,loamp,loamp,lo185upx,lo1300upx})
,.ylo({lozero,lozero,lozero,lozero,lo185upy,lo1300upy})
,.phaselo({ph185dn,ph185up,ph1300dn,ph1300up,ph185updrv,ph1300updrv})
,.phaselo0({ph185dn0,ph185up0,ph1300dn0,ph1300up0,ph185updrv0,ph1300updrv0})
);
// vectorpi cross wire
wire [5:0] freezecycle=29;
wire [WIDTH-1:0] ki=16'd10000;
wire [5:0] kishift=0;
wire [WIDTH-1:0] kp=0;
wire [5:0] kpshift=0;
wire [ZWIDTH-1:0] phdelay=17'd70000;
wire [ZWIDTH-1:0] phlodn=ph185dn[21:5];
wire [ZWIDTH-1:0] phlodn0=ph185dn0[21:5];
wire [ZWIDTH-1:0] phref=0;
wire signed [ADCWIDTH-1:0] rf_source;
wire signed [15:0] rf_source_del;
wire [19:0] cos;
wire [19:0] sin;
wire [WIDTH-1:0] vin=rf_source;//rf_source_del;
wire [WIDTH-1:0] voutx;
wire [WIDTH-1:0] vouty;
wire [WIDTH-1:0] vsetx=2000;
wire [WIDTH-1:0] vsety=3000;
wire rf_pulse=1;
wire [15:0] vclosesetx=10;
wire [15:0] vclosesety=100;
wire [15:0] vopensetx=cc<5000? 0000: 10000;
wire [15:0] vopensety=cc<5000? 10000 : 0;
wire [0:0] close_loop=1;
wire [16:0] phrot=0;//125530;
vectorpi #(.UPDN(1))vectorpi(.clk(clk)
,.close_loop(close_loop)
,.freezecycle(freezecycle)
,.ki(ki)
,.kishift(kishift)
,.kp(kp)
,.kpshift(kpshift)
,.phdelay(phdelay)
,.phlodn(phlodn)
,.phref(phref)
,.phrot(phrot)
,.reset(reset)
,.rf_pulse(rf_pulse)
,.vclosesetx(vclosesetx)
,.vclosesety(vclosesety)
,.vin(vin)
,.vopenmeasx(vopenmeasx)
,.vopenmeasy(vopenmeasy)
,.vopensetx(vopensetx)
,.vopensety(vopensety)
,.voutxfall(voutxfall)
,.voutyfall(voutyfall));
wire signed [ADCWIDTH-1:0] cos_source;
wire signed [ADCWIDTH-1:0] sin_source;
vectorpi1 #(.UPDN(0))
vectorpi1(.clk(clk),.close_loop(close_loop),.freezecycle(freezecycle),.ki(ki),.kishift(0),.kp(kp),.kpshift(10),.phdelay(phdelay),.phlodn(phlodn),.phref(phref),.phrot(phrot),.reset(cc<30),.rf_pulse(rf_pulse),.vclosesetx(1000),.vclosesety(200),.vin(vin),.vopenmeasx(vopenmeasx),.vopenmeasy(vopenmeasy),.vopensetx(vopensetx),.vopensety(vopensety),.cos(cos),.sin(sin),.sx(18011),.sy(18011),.voutx(voutx),.vouty(vouty)
//,.xrot(32767),.yrot(0)
,.xrot(14713),.yrot(-40443)
//,.vin(sin_source>>>2)
//,.voutx(voutx)
//,.voutxfall(voutxfall)
//,.vouty(vouty)
//,.voutyfall(voutyfall)
);
//assign voutx=0;
//assign vouty=0;
assign sin_source=$signed(sin185dn[LOWIDTH-1:LOWIDTH-ADCWIDTH]);
assign cos_source=$signed(cos185dn[LOWIDTH-1:LOWIDTH-ADCWIDTH]);
reg [ADCWIDTH-1:0] sin_d=0;
reg [ADCWIDTH-1:0] sin_d2=0;
reg [ADCWIDTH-1:0] sin_d3=0;
reg [ADCWIDTH-1:0] sin_d4=0;
reg [ADCWIDTH-1:0] sin_d5=0;
reg [ADCWIDTH-1:0] cos_d=0;
reg [ADCWIDTH-1:0] cos_d2=0;
reg [ADCWIDTH-1:0] cos_d3=0;
reg [ADCWIDTH-1:0] cos_d4=0;
reg [ADCWIDTH-1:0] cos_d5=0;
assign sin=sin_source;//losin;//$signed((1<<LOWIDTH-1)-1*vtsin);//sin185dn;
assign cos=cos_source;//locos;//$signed((1<<LOWIDTH-1)-1*vtcos);//cos185dn;
always @(posedge clk ) begin
	sin_d <= sin_source;
	sin_d2<=sin_d;
	sin_d3<=sin_d2;
	sin_d4<=sin_d3;
	sin_d5<=sin_d4;
	cos_d <= cos_source;
	cos_d2<=cos_d;
	cos_d3<=cos_d2;
	cos_d4<=cos_d3;
	cos_d5<=cos_d4;
	locos<=(2**LOWIDTH-1)/2*vtcos; //  2**19*0.607253*vtcos;
	losin<=(2**LOWIDTH-1)/2*vtsin; //  2**19*0.607253*vtsin;
end

//source1 #(.WIDTH(ADCWIDTH)) source1(.clk(clk),.sin(sin_source),.cos(cos_source),.d_outre(rf_source),.ampi(voutx),.ampq(vouty));
source6 #(.WIDTH(WIDTH)) source6 (.clk(clk),.cos(cos_source),.sin(sin_source),.ampi(voutx),.ampq(vouty),.d_outre(rf_source),.d_outim());
reg_delay #(.dw(16), .len(7)) ol_pipe_match(.clk(clk), .gate(1'b1),
	    .din(rf_source), .dout(rf_source_del),.reset(reset));
endmodule
