module viabus #(parameter WIDTH=4)(inout [WIDTH-1:0] a,inout [WIDTH-1:0] b);
genvar i;
generate for (i=0;i<WIDTH;i=i+1) begin
	via via(a[i],b[i]);
end
endgenerate
endmodule
